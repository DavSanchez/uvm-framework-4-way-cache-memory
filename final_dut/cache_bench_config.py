#! /usr/bin/env python
# coding=utf-8

# Importando el generador de código UVMF
import uvmf_gen

# Definiendo el paquete del test bench
# ben = uvmf_gen.BenchClass('nombreBench', 'nombreEntorno', {paramEntorno:valor})
ben = uvmf_gen.BenchClass('cache','cache',{})

# Nivel de actividad de reset del test
ben.resetAssertionLevel = True

# Parámetros del test bench
# ben.addParamDef('nombre','tipo','valor')

# Añadiendo los BFM al test bench, cada agente tendrá su BFM
# DEFINIR CON EL MISMO ORDEN QUE EN EL ENTORNO!! (Se define como un array... ver docs)
# ben.addBfm('nombreAgente','tipoAgente','reloj','reset','actividad',{paramBFM:valor})
ben.addBfm('cpu_agent','cpu','clk','rst', 'ACTIVE')
ben.addBfm('mem_agent','mem','clk','rst', 'ACTIVE')

# Creando el test bench
ben.create()
