`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:46:20 11/20/2017 
// Design Name: 
// Module Name:    cacheTestBench
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
	
module cacheTestbench;
  
  reg clk;
  reg reset;
  reg [127:0] inputBlock;
  reg [7:0] byteToWrite;

  reg [31:0] address;

  reg memWrite, memRead;
  wire hit, dirty, valid;
  wire [1:0] waySel;
  wire [23:0] tag_out;
  wire [127:0] replacementBlock;
  wire [7:0] byteDataOutFromCache;
  wire writeBackSignal;
  
  Cache cache_1(.clk(clk), .reset(reset), .byteToWrite(byteToWrite),
  .inputBlock(inputBlock), .address(address), .memWrite(memWrite), 
  .memRead(memRead), .hit(hit),.valid(valid), .dirty(dirty), .tag_out(tag_out), .waySel(waySel), 
  .replacementBlock(replacementBlock), .byteDataOutFromCache(byteDataOutFromCache), 
  .writeBackSignal(writeBackSignal));
  
  always
    #5 clk=~clk;
  initial
    begin
      clk=0; reset=1;
      #10  reset=0;
      
      #10
      
      /*
      1. Cache Miss (READ) Set 1  (Data gets written to way1)
      physicalAddressToCache = 32'h00_11_22_33; 
      READ MISS - must take a block from memory and read from that block in the cache
      */
      memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0001_0001_0010_0010_0011_0011;
      
      //16 B data coming from main memory
	   #10
      inputBlock = 128'hAA_BB_CC_DD_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
      
      //byteToWrite is a don't care as it is a memRead signal
      byteToWrite = 8'd0;
		
		#30
		//2. Changing only the tag; The way should change; this should again be a miss initially
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0001_0001_0011_0010_0011_0011; 
      
      //16 B data (that is actually supposed to be in the memory at the above address)
	   #10
      inputBlock = 128'hBB_BB_CC_DD_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
		
		#30
		//3. Using the first address again; should give a hit.
		
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0001_0001_0011_0010_0011_0011;
		
		//4. Miss again in the same set.
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0001_0011_0011_0010_0011_0011; 
      
      //16 B data (that is actually supposed to be in the memory at the above address)
	   #10
      inputBlock = 128'hBB_BB_CC_EE_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
		 
		//5. Again miss in the same set.
		#30
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0001_0011_0011_0011_0011_0011; 
      
      //16 B data (that is actually supposed to be in the memory at the above address)
	   #10
      inputBlock = 128'hBB_BB_CC_EE_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
		
		//6. Again miss in the same set. This time it should replace the 00 way by the lru policy.
		#30
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0011_0011_0011_0010_0011_0011; 
      
      //16 B data (that is actually supposed to be in the memory at the above address)
	   #10
      inputBlock = 128'hBB_BB_DD_EE_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
		
		//7. Hit
		#30
		memRead = 1;  
      memWrite = 0;
		address = 32'b0000_0000_0011_0011_0011_0010_0011_0011; 
      
      //16 B data (that is actually supposed to be in the memory at the above address)
	   #10
      inputBlock = 128'hBB_BB_DD_EE_EE_00_11_22_AA_BB_CC_DD_EE_00_11_22;
	  
      #50 $finish; 
    end
    
endmodule	
