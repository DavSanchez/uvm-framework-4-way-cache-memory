# Cache-Implementation

## Cache specs

- 4-way Set Associative
- Way halting
- Write Back Policy
- LRU Counter Replacement Policy
- 1KB Cache Size
- 16B Line Size

## Tasks

- [x] Generate Python configuration files for agents
  - [x] Agent for CPU I/O
  - [x] Agent for Memory I/O
- [x] Add SV protocol specific code to agents
  - [x] `_driver_bfm.sv`
  - [x] `_monitor_bfm.sv`
  - [x] Editing responder sequence
- [x] Generate Python configuration file for environment
- [ ] Add SV specific code to environment
    - [ ] Implemented predictor behavior
- [x] Generate Python configuration file for testbench
- [ ] Add SV specific code to testbench
    - [x] Editing `_sequence_base`.
    - [x] New sequences extended from `_sequence_base`
      - [x] **Read Sequence:**
        - [x] Created new sequence in CPU agent
        - [x] Created new virtual sequence in bench sequences
        - [x] Created new test that uses the new sequence
      - [x] **Write Sequence:**
        - [x] Created new sequence in CPU agent
        - [x] Created new virtual sequence in bench sequences
        - [x] Created new test that uses the new sequence
      - [x] **Write-Back Sequence:**
        - [x] Created new sequence in CPU agent
        - [x] Created new virtual sequence in bench sequences
        - [x] Created new test that uses the new sequence
        - [x] **Intended behavior:**
          - [x] Read and write four consecutive lines, so DUT completely loads and updates a set
          - [x] Read a fifth line to check if `write_back` signal raises.


## DUT connectivity\*

![Cache DUT proposed connectivity](dut_assets/DUT.jpg)
(\*)Excluding `clk` and `reset` signals.

[//]: # (Leer cuatro líneas consecutivas, para que las cuatro se depositen en el mismo index de la caché.)
[//]: # (Modificar, mediante cuatro escrituras consecutivas, cada una de esas cuatro líneas.)
[//]: # (Leer una quinta línea a la que le corresponda el mismo index. Se debe observar como se activa el WriteBack.)
