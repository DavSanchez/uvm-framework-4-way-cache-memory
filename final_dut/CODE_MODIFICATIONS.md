# Code snippets for environment

## Memory Agent

### mem_driver_bfm.svh
#### task `do_response()``
```sv
    // TODO: CHANGED
    input_block_o <= input_block;
```
#### Change input for output for input_block in do_response and response() tasks

### mem_monitor_bfm.svh
#### task do_monitor()
```sv
      // TODO: CHANGED
      //-start_time = $time;
      address = address_i;    //    [ADDRESS_WIDTH-1:0] 
      mem_write = mem_write_i;//     
      mem_read = mem_read_i;  //
      input_block = input_block_i;
      valid = valid_i;
      dirty = dirty_i;
      way_sel = way_sel_i;
      tag_out = tag_out_i;
      replacement_block = replacement_block_i;
      write_back = write_back_i;
      //-end_time = $time;
```

### mem_responder_sequence.svh
#### variables
```sv
  // TODO: CHANGED
  bit [BLOCK_WIDTH-1:0] memory [bit[ADDRESS_WIDTH-1:ADDR_WORD_FIELD_WIDTH-1]];
  rand bit [BLOCK_WIDTH-1:0] data;
  bit [ADDR_TAG_FIELD_WIDTH+ADDR_INDEX_FIELD_WIDTH-1:0] index;
```
#### task body()
```sv
      // TODO: CHANGED
      $display("cache_mem_responder_sequence: Evaluating R/W signals...");
      $display("cache_mem_responder_sequence: mem_read value: %h", req.mem_read);
      $display("cache_mem_responder_sequence: mem_write value: %h", req.mem_write);
      $display("cache_mem_responder_sequence: address value: %h", req.address);
      if(req.mem_read != req.mem_write) begin
        $display("Entered response condition!!");
        // Truncamos los bits menos significativos de la dirección para obviar el campo WORD
        index = req.address[ADDRESS_WIDTH-1:ADDR_WORD_FIELD_WIDTH-1];
        // Chequeamos el índice en el array asociativo, si no existe el elemento lo creamos y almacenamos
        if(!memory.exists(index)) begin
          std::randomize(data);
          // consider the insert method: memory.insert(index, data);
          memory[index] = data;
        end
        req.input_block = memory[index];
      end
      `uvm_info("SEQ",$sformatf("Processed txn: %s",req.convert2string()),UVM_HIGH)
```

## CPU Agent

### cpu_driver_bfm.svh
#### task do_transfer() and additionals (until do_response())
```sv
  // TODO: CHANGED
  address_o <= {address_tag,address_index,address_word};
  mem_read_o <= mem_read;
  mem_write_o <= mem_write;
  byte_to_write_o <= byte_to_write;
  while (!hit_i) @(posedge clk_i);
```

### cpu_monitor_bfm.svh
#### task do_monitor()
```sv
      //TODO: CHANGED
      //-start_time = $time; //TODO: Esto es para algo?
      address = address_i;
      hit = hit_i;
      mem_read = mem_read_i;
      mem_write = mem_write_i;
      byte_to_write = byte_to_write_i;
      byte_data_out_from_cache = byte_data_out_from_cache_i;
      //-end_time = $time; //TODO: Esto es para algo?
```

## Bench
### cache_bench_sequence_base.svh
#### below // Instantiate sequences here
```sv
  // TODO: CHANGED. Instantiate RESPONDER sequence for memory agent
typedef mem_responder_sequence mem_agent_responder_seq_t;
mem_agent_responder_seq_t mem_agent_responder_seq;
```
#### below // Construct sequences here
```sv
  // TODO: CHANGED. Constructed responder sequence for memory
   mem_agent_responder_seq  = mem_agent_responder_seq_t::type_id::create("mem_agent_responder_seq");
```

#### inside // Start RESPONDER sequences here
```sv
  // Start RESPONDER sequences here
   fork
    mem_agent_responder_seq.start(mem_agent_sequencer);
   join_none
```
#### inside // Start INITIATOR sequences here
```sv
    //repeat (25) mem_agent_random_seq.start(mem_agent_sequencer);
```

### cache/sim/Makefile
#### DUT declaration
```
uncomment DUT line
```

### cache/rtl/verilog
```
Place cache.v there
```

### hdl_top.svh
#### In mem_if  mem_agent_bus(.clk(clk), .rst(rst));
Añadir puertos para que use las entradas del `cpu_agent_bus` donde haga falta (`address`, `mem_write`, `mem_read`). (XX ??)
#### In // Instantiate DUT here
```sv
Cache #(
   // .WORD_WIDTH(8),
   // .ADDRESS_WIDTH(32),
   // .BLOCK_WIDTH(128),
   // .ADDR_WORD_FIELD_WIDTH(4),
   // .ADDR_INDEX_FIELD_WIDTH(4),
   // .ADDR_TAG_FIELD_WIDTH(24),
   // .WAY_SEL_WIDTH(2)
   ) DUT (
   .clk                    (cpu_agent_bus.clk),
   .reset                  (cpu_agent_bus.rst),
   .byteToWrite            (cpu_agent_bus.byte_to_write),
   .inputBlock             (mem_agent_bus.input_block),
   .address                (cpu_agent_bus.address),
   .memWrite               (cpu_agent_bus.mem_write),
   .memRead                (cpu_agent_bus.mem_read),
   .hit                    (cpu_agent_bus.hit),
   .valid                  (mem_agent_bus.valid),
   .dirty                  (mem_agent_bus.dirty),
   .waySel                 (mem_agent_bus.way_sel),
   .tag_out                (mem_agent_bus.tag_out),
   .replacementBlock       (mem_agent_bus.replacement_block),
   .byteDataOutFromCache   (cpu_agent_bus.byte_data_out_from_cache),
   .writeBackSignal        (mem_agent_bus.write_back)
);
```

### cpu_transaction.svh y mem_transaction.svh

#### inside add_to_wave()
```sv
// TODO: CHANGED. Added colors to read and write transactions from CPU
if (mem_read == 1'b1 && mem_write == 1'b0) begin
  $add_color(transaction_view_h,"green");
end else if (mem_read == 1'b0 && mem_write == 1'b1) begin
  $add_color(transaction_view_h,"orange");
end
```

### cpu_random_sequence.svh
#### Añadir inline constraint justo después de !req.randomize()
```sv
with {req.mem_read != req.mem_write;}
```