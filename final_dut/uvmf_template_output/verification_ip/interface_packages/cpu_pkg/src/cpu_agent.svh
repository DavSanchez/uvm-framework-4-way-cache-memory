//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface UVM agent
// File            : cpu_agent.svh
//----------------------------------------------------------------------
//     
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_agent #( int WORD_WIDTH = 8, int ADDRESS_WIDTH = 32, int ADDR_WORD_FIELD_WIDTH = 4, int ADDR_INDEX_FIELD_WIDTH = 4, int ADDR_TAG_FIELD_WIDTH = 24)extends uvmf_parameterized_agent #(
                    .CONFIG_T(cpu_configuration #(.WORD_WIDTH(WORD_WIDTH),.ADDRESS_WIDTH(ADDRESS_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .DRIVER_T(cpu_driver #(.WORD_WIDTH(WORD_WIDTH),.ADDRESS_WIDTH(ADDRESS_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .MONITOR_T(cpu_monitor #(.WORD_WIDTH(WORD_WIDTH),.ADDRESS_WIDTH(ADDRESS_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .COVERAGE_T(cpu_transaction_coverage #(.WORD_WIDTH(WORD_WIDTH),.ADDRESS_WIDTH(ADDRESS_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .TRANS_T(cpu_transaction #(.WORD_WIDTH(WORD_WIDTH),.ADDRESS_WIDTH(ADDRESS_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)))
                    );

  `uvm_component_param_utils (cpu_agent #(WORD_WIDTH,ADDRESS_WIDTH,ADDR_WORD_FIELD_WIDTH,ADDR_INDEX_FIELD_WIDTH,ADDR_TAG_FIELD_WIDTH) )

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

endclass
