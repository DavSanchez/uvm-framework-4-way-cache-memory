//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface UVM monitor
// File            : cpu_monitor.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class receives cpu transactions observed by the
//     cpu monitor BFM and broadcasts them through the analysis port
//     on the agent. It accesses the monitor BFM through the monitor
//     task. This UVM component captures transactions
//     for viewing in the waveform viewer if the
//     enable_transaction_viewing flag is set in the configuration.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_monitor  #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_monitor_base #(
                    .CONFIG_T(cpu_configuration  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                    .BFM_BIND_T(virtual cpu_monitor_bfm  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                    .TRANS_T(cpu_transaction  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ));

  `uvm_component_param_utils( cpu_monitor #(
                              WORD_WIDTH,
                              ADDRESS_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

// ****************************************************************************
   virtual function void configure(input CONFIG_T cfg);
      bfm.configure(

          cfg.active_passive,
          cfg.initiator_responder
);                    
   
   endfunction

// ****************************************************************************
   virtual function void set_bfm_proxy_handle();
      bfm.proxy = this;
   endfunction

 // ****************************************************************************              
  virtual task run_phase(uvm_phase phase);                                                   
  // Start monitor BFM thread and don't call super.run() in order to                       
  // override the default monitor proxy 'pull' behavior with the more                      
  // emulation-friendly BFM 'push' approach using the notify_transaction                   
  // function below                                                                        
  bfm.start_monitoring();                                                   
  endtask                                                                                    
  
  // ****************************************************************************              
  virtual function void notify_transaction(
                        input byte unsigned byte_to_write,  
                        input bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,  
                        input bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,  
                        input bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,  
                        input bit [ADDRESS_WIDTH-1:0] address,  
                        input bit mem_write,  
                        input bit mem_read,  
                        input bit hit,  
                        input byte unsigned byte_data_out_from_cache 
                        );
    trans = new("trans");                                                                   
    trans.start_time = time_stamp;                                                          
    trans.end_time = $time;                                                                 
    time_stamp = trans.end_time;                                                            
    trans.byte_to_write = byte_to_write;
    trans.address_word = address_word;
    trans.address_index = address_index;
    trans.address_tag = address_tag;
    trans.address = address;
    trans.mem_write = mem_write;
    trans.mem_read = mem_read;
    trans.hit = hit;
    trans.byte_data_out_from_cache = byte_data_out_from_cache;
    analyze(trans);                                                                         
  endfunction  

endclass
