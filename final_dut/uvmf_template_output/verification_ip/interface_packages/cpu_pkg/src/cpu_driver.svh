//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface UVM Driver
// File            : cpu_driver.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class passes transactions between the sequencer
//        and the BFM driver interface.  It accesses the driver BFM 
//        through the bfm handle. This driver
//        passes transactions to the driver BFM through the access
//        task.  
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_driver  #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_driver_base #(
                   .CONFIG_T(cpu_configuration  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .BFM_BIND_T(virtual cpu_driver_bfm #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .REQ(cpu_transaction  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .RSP(cpu_transaction  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ));

  `uvm_component_param_utils( cpu_driver #(
                              WORD_WIDTH,
                              ADDRESS_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent=null );
    super.new( name, parent );
  endfunction

// ****************************************************************************
   virtual function void configure(input CONFIG_T cfg);
      bfm.configure(

          cfg.active_passive,
          cfg.initiator_responder
);                    
   
   endfunction

// ****************************************************************************
   virtual function void set_bfm_proxy_handle();
      bfm.proxy = this;
   endfunction

// ****************************************************************************              
  virtual task access( inout REQ txn );
      if (configuration.initiator_responder==RESPONDER) begin
        if (1'b1) begin
          bfm.do_response_ready(
                      );
        end
        bfm.response(
      txn.byte_to_write,
      txn.address_word,
      txn.address_index,
      txn.address_tag,
      txn.address,
      txn.mem_write,
      txn.mem_read,
      txn.hit,
      txn.byte_data_out_from_cache        
          );
      end else begin    
        bfm.access(
      txn.byte_to_write,
      txn.address_word,
      txn.address_index,
      txn.address_tag,
      txn.address,
      txn.mem_write,
      txn.mem_read,
      txn.hit,
      txn.byte_data_out_from_cache            );
    end
  endtask

endclass
