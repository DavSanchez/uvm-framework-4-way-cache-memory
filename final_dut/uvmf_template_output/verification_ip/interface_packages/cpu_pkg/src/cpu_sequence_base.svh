//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Sequence Base
// File            : cpu_sequence_base.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
// This file contains the class used as the base class for all sequences
// for this interface.
//
// ****************************************************************************
// ****************************************************************************
class cpu_sequence_base  #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_sequence_base #(
                             .REQ(cpu_transaction  #(
                                 .WORD_WIDTH(WORD_WIDTH),                                
                                 .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                                 .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                                 .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                                 .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                             .RSP(cpu_transaction  #(
                                 .WORD_WIDTH(WORD_WIDTH),                                
                                 .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                                 .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                                 .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                                 .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ));

  `uvm_object_param_utils( cpu_sequence_base #(
                              WORD_WIDTH,
                              ADDRESS_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

  // variables
  cpu_transaction  #(
                     .WORD_WIDTH(WORD_WIDTH),                                
                     .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                     .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                     .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                     .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                     )  req;
  cpu_transaction  #(
                     .WORD_WIDTH(WORD_WIDTH),                                
                     .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                     .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                     .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                     .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                     )  rsp;

// Event for identifying when a response was received from the sequencer
event new_rsp;
// ****************************************************************************
// TASK : get_responses()
// This task recursively gets sequence item responses from the sequencer.
//
virtual task get_responses();
   fork
      begin
         // Block until new rsp available
         get_response(rsp);
         // New rsp received.  Indicate to sequence using event.
         ->new_rsp;
         // Display the received response transaction
         `uvm_info("SEQ", {"New response transaction:",rsp.convert2string()}, UVM_MEDIUM)
      end
   join_none
endtask

// ****************************************************************************
// TASK : pre_body()
// This task is called automatically when start is called with call_pre_post set to 1 (default).
// By calling get_responses() within pre_body() any derived sequences are automatically 
// processing response transactions.
//
virtual task pre_body();
   get_responses();
endtask

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name ="");
    super.new( name );
  endfunction

endclass
//----------------------------------------------------------------------
//
