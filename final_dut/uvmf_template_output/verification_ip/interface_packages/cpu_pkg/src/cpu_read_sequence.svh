//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 09
// Created from cpu_random_sequence
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface read sequence
// File            : cpu_read_sequence.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
// This sequences randomizes a cpu read transaction and sends it 
// to the UVM driver.
//
// ****************************************************************************
// This sequence constructs and randomizes a read cpu_transaction.
// 
class cpu_read_sequence  #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
extends cpu_sequence_base  #(
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ;

  `uvm_object_param_utils( cpu_read_sequence #(
                           WORD_WIDTH,
                           ADDRESS_WIDTH,
                           ADDR_WORD_FIELD_WIDTH,
                           ADDR_INDEX_FIELD_WIDTH,
                           ADDR_TAG_FIELD_WIDTH
                            ))

//*****************************************************************
  function new(string name = "");
    super.new(name);
  endfunction: new

// ****************************************************************************
// TASK : body()
// This task is automatically executed when this sequence is started using the 
// start(sequencerHandle) task.
//
  task body();

    begin
      // Construct the transaction
      req=cpu_transaction #( 
                .WORD_WIDTH(WORD_WIDTH),                                
                .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
               ) ::type_id::create("req");

      start_item(req);
      // Randomize the transaction
      if(!req.randomize() with {req.mem_read == 1'b1 && req.mem_write == 1'b0;}) `uvm_fatal("SEQ", "cpu_read_sequence::body()-cpu_transaction randomization failed")
      // Send the transaction to the cpu_driver_bfm via the sequencer and cpu_driver.
      finish_item(req);
      `uvm_info("SEQ", {"Response:",req.convert2string()},UVM_MEDIUM)
    end

  endtask: body

endclass: cpu_read_sequence
//----------------------------------------------------------------------
//
