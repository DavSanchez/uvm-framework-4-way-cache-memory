//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Transaction Coverage
// File            : cpu_transaction_coverage.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class records cpu transaction information using
//       a covergroup named cpu_transaction_cg.  An instance of this
//       coverage component is instantiated in the uvmf_parameterized_agent
//       if the has_coverage flag is set.
//
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_transaction_coverage  #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvm_subscriber #(.T(cpu_transaction  #(
                                            .WORD_WIDTH(WORD_WIDTH),                                
                                            .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                                            .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                                            .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                                            .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                                            ) ));

  `uvm_component_param_utils( cpu_transaction_coverage #(
                              WORD_WIDTH,
                              ADDRESS_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

  byte unsigned byte_to_write;
  bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word;
  bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index;
  bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag;
  bit [ADDRESS_WIDTH-1:0] address;
  bit mem_write;
  bit mem_read;
  bit hit;
  byte unsigned byte_data_out_from_cache;

// ****************************************************************************
  // UVMF_CHANGE_ME : Add coverage bins, crosses, exclusions, etc. according to coverage needs.
  covergroup cpu_transaction_cg;
    option.auto_bin_max=1024;
    coverpoint byte_to_write;
    coverpoint address_word;
    coverpoint address_index;
    coverpoint address_tag;
    coverpoint address;
    coverpoint mem_write;
    coverpoint mem_read;
    coverpoint hit;
    coverpoint byte_data_out_from_cache;
  endgroup

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new(string name="", uvm_component parent=null);
    super.new(name,parent);
    cpu_transaction_cg=new;
    cpu_transaction_cg.set_inst_name($sformatf("cpu_transaction_cg_%s",get_full_name()));
 endfunction

// ****************************************************************************
// FUNCTION: write (T t)
// This function is automatically executed when a transaction arrives on the
// analysis_export.  It copies values from the variables in the transaction 
// to local variables used to collect functional coverage.  
//
  virtual function void write (T t);
    `uvm_info("COV","Received transaction",UVM_HIGH);
    byte_to_write = t.byte_to_write;
    address_word = t.address_word;
    address_index = t.address_index;
    address_tag = t.address_tag;
    address = t.address;
    mem_write = t.mem_write;
    mem_read = t.mem_read;
    hit = t.hit;
    byte_data_out_from_cache = t.byte_data_out_from_cache;
    cpu_transaction_cg.sample();
  endfunction

endclass
