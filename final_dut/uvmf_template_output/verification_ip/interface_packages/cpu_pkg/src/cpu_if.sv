//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Signal Bundle
// File            : cpu_if.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: This interface contains the cpu interface signals.
//      It is instantiated once per cpu bus.  Bus Functional Models, 
//      BFM's named cpu_driver_bfm, are used to drive signals on the bus.
//      BFM's named cpu_monitor_bfm are used to monitor signals on the 
//      bus. This interface signal bundle is passed in the port list of
//      the BFM in order to give the BFM access to the signals in this
//      interface.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
// This template can be used to connect a DUT to these signals
//
// .dut_signal_port(cpu_bus.byte_to_write), // Agent output 
// .dut_signal_port(cpu_bus.address), // Agent output 
// .dut_signal_port(cpu_bus.mem_write), // Agent output 
// .dut_signal_port(cpu_bus.mem_read), // Agent output 
// .dut_signal_port(cpu_bus.hit), // Agent input 
// .dut_signal_port(cpu_bus.byte_data_out_from_cache), // Agent input 

import uvmf_base_pkg_hdl::*;
import cpu_pkg_hdl::*;

interface  cpu_if       #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
 ( 
  input tri clk, 
  input tri rst
  ,inout tri [WORD_WIDTH-1:0] byte_to_write
  ,inout tri [ADDRESS_WIDTH-1:0] address
  ,inout tri  mem_write
  ,inout tri  mem_read
  ,inout tri  hit
  ,inout tri [WORD_WIDTH-1:0] byte_data_out_from_cache
);

endinterface

