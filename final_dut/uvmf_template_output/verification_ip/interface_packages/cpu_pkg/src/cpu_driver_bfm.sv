//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Driver BFM
// File            : cpu_driver_bfm.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
//    This interface performs the cpu signal driving.  It is
//     accessed by the uvm cpu driver through a virtual interface
//     handle in the cpu configuration.  It drives the singals passed
//     in through the port connection named bus of type cpu_if.
//
//     Input signals from the cpu_if are assigned to an internal input
//     signal with a _i suffix.  The _i signal should be used for sampling.
//
//     The input signal connections are as follows:
//       bus.signal -> signal_i 
//
//     This bfm drives signals with a _o suffix.  These signals
//     are driven onto signals within cpu_if based on INITIATOR/RESPONDER and/or
//     ARBITRATION/GRANT status.  
//
//     The output signal connections are as follows:
//        signal_o -> bus.signal
//
//                                                                                           
//      Interface functions and tasks used by UVM components:                                
//             configure(uvmf_initiator_responder_t mst_slv);                                       
//                   This function gets configuration attributes from the                    
//                   UVM driver to set any required BFM configuration                        
//                   variables such as 'initiator_responder'.                                       
//                                                                                           
//             access(
//       byte unsigned byte_to_write,
//       bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
//       bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
//       bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
//       bit [ADDRESS_WIDTH-1:0] address,
//       bit mem_write,
//       bit mem_read,
//       bit hit,
//       byte unsigned byte_data_out_from_cache );//                   );
//                   This task receives transaction attributes from the                      
//                   UVM driver and then executes the corresponding                          
//                   bus operation on the bus. 
//
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
import uvmf_base_pkg_hdl::*;
import cpu_pkg_hdl::*;

interface cpu_driver_bfm       #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
(cpu_if  bus);
// pragma attribute cpu_driver_bfm partition_interface_xif
// The above pragma and additional ones in-lined below are for running this BFM on Veloce

  // Config value to determine if this is an initiator or a responder 
  uvmf_initiator_responder_t initiator_responder;

  tri        clk_i;
  tri        rst_i;

// Signal list (all signals are capable of being inputs and outputs for the sake
// of supporting both INITIATOR and RESPONDER mode operation. Expectation is that 
// directionality in the config file was from the point-of-view of the INITIATOR

// INITIATOR mode input signals
  tri         hit_i;
  bit         hit_o;
  tri       [WORD_WIDTH-1:0]  byte_data_out_from_cache_i;
  bit       [WORD_WIDTH-1:0]  byte_data_out_from_cache_o;

// INITIATOR mode output signals
  tri       [WORD_WIDTH-1:0]  byte_to_write_i;
  bit       [WORD_WIDTH-1:0]  byte_to_write_o;
  tri       [ADDRESS_WIDTH-1:0]  address_i;
  bit       [ADDRESS_WIDTH-1:0]  address_o;
  tri         mem_write_i;
  bit         mem_write_o;
  tri         mem_read_i;
  bit         mem_read_o;

// Bi-directional signals
  

  assign     clk_i    =   bus.clk;
  assign     rst_i    =   bus.rst;

  // These are signals marked as 'input' by the config file, but the signals will be
  // driven by this BFM if put into RESPONDER mode (flipping all signal directions around)
  assign     hit_i = bus.hit;
  assign     bus.hit = (initiator_responder == RESPONDER) ? hit_o : 'bz;
  assign     byte_data_out_from_cache_i = bus.byte_data_out_from_cache;
  assign     bus.byte_data_out_from_cache = (initiator_responder == RESPONDER) ? byte_data_out_from_cache_o : 'bz;


  // These are signals marked as 'output' by the config file, but the outputs will
  // not be driven by this BFM unless placed in INITIATOR mode.
  assign bus.byte_to_write = (initiator_responder == INITIATOR) ? byte_to_write_o : 'bz;
  assign byte_to_write_i = bus.byte_to_write;
  assign bus.address = (initiator_responder == INITIATOR) ? address_o : 'bz;
  assign address_i = bus.address;
  assign bus.mem_write = (initiator_responder == INITIATOR) ? mem_write_o : 'bz;
  assign mem_write_i = bus.mem_write;
  assign bus.mem_read = (initiator_responder == INITIATOR) ? mem_read_o : 'bz;
  assign mem_read_i = bus.mem_read;

   // Proxy handle to UVM driver
   cpu_pkg::cpu_driver  #(
              .WORD_WIDTH(WORD_WIDTH),                                
              .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
              .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
              .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
              .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                    )  proxy;
  // pragma tbx oneway proxy.my_function_name_in_uvm_driver                 

//******************************************************************                         
   function void configure(
          uvmf_active_passive_t active_passive,
          uvmf_initiator_responder_t   init_resp
); // pragma tbx xtf                   
      initiator_responder = init_resp;
   
   endfunction                                                                               


// ****************************************************************************
  task do_transfer(                input byte unsigned byte_to_write,
                input bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
                input bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
                input bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
                input bit [ADDRESS_WIDTH-1:0] address,
                input bit mem_write,
                input bit mem_read,
                input bit hit,
                input byte unsigned byte_data_out_from_cache               );                                                  
  // UVMF_CHANGE_ME : Implement protocol signaling.
  // Transfers are protocol specific and therefore not generated by the templates.
  // Use the following as examples of transferring data between a sequence and the bus
  // In the wb_pkg - wb_master_access_sequence.svh, wb_driver_bfm.sv
  // Reference code;
  //    while (control_signal == 1'b1) @(posedge clk_i);
  //    INITIATOR mode input signals
  //    hit_i;        //    
  //    hit_o <= xyz; //     
  //    byte_data_out_from_cache_i;        //   [WORD_WIDTH-1:0] 
  //    byte_data_out_from_cache_o <= xyz; //   [WORD_WIDTH-1:0]  
  //    INITIATOR mode output signals
  //    byte_to_write_i;        //   [WORD_WIDTH-1:0]  
  //    byte_to_write_o <= xyz; //   [WORD_WIDTH-1:0]  
  //    address_i;        //   [ADDRESS_WIDTH-1:0]  
  //    address_o <= xyz; //   [ADDRESS_WIDTH-1:0]  
  //    mem_write_i;        //     
  //    mem_write_o <= xyz; //     
  //    mem_read_i;        //     
  //    mem_read_o <= xyz; //     
  //    Bi-directional signals
  
  // TODO: CHANGED
  address_o <= {address_tag,address_index,address_word};
  mem_read_o <= mem_read;
  mem_write_o <= mem_write;
  byte_to_write_o <= byte_to_write;
  while (!hit_i) @(posedge clk_i);
  $display("cpu_driver_bfm: Inside do_transfer()");
endtask        

  // UVMF_CHANGE_ME : Implement response protocol signaling.
  // Templates also do not generate protocol specific response signaling. Use the 
  // following as examples for transferring data between a sequence and the bus
  // In wb_pkg - wb_memory_slave_sequence.svh, wb_driver_bfm.sv

  task do_response(                 output byte unsigned byte_to_write,
                 output bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
                 output bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
                 output bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
                 output bit [ADDRESS_WIDTH-1:0] address,
                 output bit mem_write,
                 output bit mem_read,
                 output bit hit,
                 output byte unsigned byte_data_out_from_cache       );
    @(posedge clk_i);
    @(posedge clk_i);
    @(posedge clk_i);
    @(posedge clk_i);
    @(posedge clk_i);
  endtask

  // The resp_ready bit is intended to act as a simple event scheduler and does
  // not have anything to do with the protocol. It is intended to be set by
  // a proxy call to do_response_ready() and ultimately cleared somewhere within the always
  // block below.  In this simple situation, resp_ready will be cleared on the
  // clock cycle immediately following it being set.  In a more complex protocol,
  // the resp_ready signal could be an input to an explicit FSM to properly
  // time the responses to transactions.  
  bit resp_ready;
  always @(posedge clk_i) begin
    if (resp_ready) begin
      resp_ready <= 1'b0;
    end
  end

  function void do_response_ready(    );  // pragma tbx xtf
    // UVMF_CHANGE_ME : Implement response - drive BFM outputs based on the arguments
    // passed into this function.  IMPORTANT - Must not consume time (it must remain
    // a function)
    resp_ready <= 1'b1;
  endfunction

// ****************************************************************************              
// UVMF_CHANGE_ME : Note that all transaction variables are passed into the access
//   task as inputs.  Some of these may need to be changed to outputs based on
//   protocol needs.
//
  task access(
    input   byte unsigned byte_to_write,
    input   bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
    input   bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
    input   bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
    input   bit [ADDRESS_WIDTH-1:0] address,
    input   bit mem_write,
    input   bit mem_read,
    input   bit hit,
    input   byte unsigned byte_data_out_from_cache );
  // pragma tbx xtf                    
  @(posedge clk_i);                                                                     
  $display("cpu_driver_bfm: Inside access()");
  do_transfer(
    byte_to_write,
    address_word,
    address_index,
    address_tag,
    address,
    mem_write,
    mem_read,
    hit,
    byte_data_out_from_cache          );                                                  
  endtask      

// ****************************************************************************              
// UVMF_CHANGE_ME : Note that all transaction variables are passed into the response
//   task as outputs.  Some of these may need to be changed to inputs based on
//   protocol needs.
  task response(
 output byte unsigned byte_to_write,
 output bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
 output bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
 output bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
 output bit [ADDRESS_WIDTH-1:0] address,
 output bit mem_write,
 output bit mem_read,
 output bit hit,
 output byte unsigned byte_data_out_from_cache );
  // pragma tbx xtf
     @(posedge clk_i);
     $display("cpu_driver_bfm: Inside response()");
    do_response(
      byte_to_write,
      address_word,
      address_index,
      address_tag,
      address,
      mem_write,
      mem_read,
      hit,
      byte_data_out_from_cache        );
  endtask             
  
endinterface
