//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Monitor BFM
// File            : cpu_monitor_bfm.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: This interface performs the cpu signal monitoring.
//      It is accessed by the uvm cpu monitor through a virtual
//      interface handle in the cpu configuration.  It monitors the
//      signals passed in through the port connection named bus of
//      type cpu_if.
//
//     Input signals from the cpu_if are assigned to an internal input
//     signal with a _i suffix.  The _i signal should be used for sampling.
//
//     The input signal connections are as follows:
//       bus.signal -> signal_i 
//
//      Interface functions and tasks used by UVM components:
//             monitor(inout TRANS_T txn);
//                   This task receives the transaction, txn, from the
//                   UVM monitor and then populates variables in txn
//                   from values observed on bus activity.  This task
//                   blocks until an operation on the cpu bus is complete.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
import uvmf_base_pkg_hdl::*;
import cpu_pkg_hdl::*;

interface cpu_monitor_bfm       #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
( cpu_if  bus );
// pragma attribute cpu_monitor_bfm partition_interface_xif                                  
// The above pragma and additional ones in-lined below are for running this BFM on Veloce


   tri        clk_i;
   tri        rst_i;
   tri        [WORD_WIDTH-1:0] byte_to_write_i;
   tri        [ADDRESS_WIDTH-1:0] address_i;
   tri         mem_write_i;
   tri         mem_read_i;
   tri         hit_i;
   tri        [WORD_WIDTH-1:0] byte_data_out_from_cache_i;

   assign     clk_i    =   bus.clk;
   assign     rst_i    =   bus.rst;
   assign     byte_to_write_i = bus.byte_to_write;
   assign     address_i = bus.address;
   assign     mem_write_i = bus.mem_write;
   assign     mem_read_i = bus.mem_read;
   assign     hit_i = bus.hit;
   assign     byte_data_out_from_cache_i = bus.byte_data_out_from_cache;

   // Proxy handle to UVM monitor
   cpu_pkg::cpu_monitor  #(
              .WORD_WIDTH(WORD_WIDTH),                                
              .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
              .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
              .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
              .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                    )  proxy;
  // pragma tbx oneway proxy.notify_transaction                 

//******************************************************************                         
   task wait_for_reset(); // pragma tbx xtf                                                  
      @(posedge clk_i) ;                                                                    
      do_wait_for_reset();                                                                   
   endtask                                                                                   

// ****************************************************************************              
   task do_wait_for_reset();                                                                 
      wait ( rst_i == 0 ) ;                                                              
      @(posedge clk_i) ;                                                                    
   endtask    
   
//******************************************************************                         
   task wait_for_num_clocks( input int unsigned count); // pragma tbx xtf                           
      @(posedge clk_i);                                                                     
      repeat (count-1) @(posedge clk_i);                                                    
   endtask      

//******************************************************************                         
  event go;                                                                                 
  function void start_monitoring(); // pragma tbx xtf      
     -> go;                                                                                 
  endfunction                                                                               
  
  // ****************************************************************************              
  initial begin                                                                             
     @go;                                                                                   
     forever begin                                                                          
        byte unsigned byte_to_write;
        bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word;
        bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index;
        bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag;
        bit [ADDRESS_WIDTH-1:0] address;
        bit mem_write;
        bit mem_read;
        bit hit;
        byte unsigned byte_data_out_from_cache;
        @(posedge clk_i);                                                                   

        do_monitor(
                   byte_to_write,
                   address_word,
                   address_index,
                   address_tag,
                   address,
                   mem_write,
                   mem_read,
                   hit,
                   byte_data_out_from_cache                  );
        proxy.notify_transaction(
                   byte_to_write,
                   address_word,
                   address_index,
                   address_tag,
                   address,
                   mem_write,
                   mem_read,
                   hit,
                   byte_data_out_from_cache                                );     
     end                                                                                    
  end                                                                                       

//******************************************************************
   function void configure(
          uvmf_active_passive_t active_passive,
          uvmf_initiator_responder_t   initiator_responder
); // pragma tbx xtf

   endfunction


// ****************************************************************************              
     task do_monitor(
                   output byte unsigned byte_to_write,
                   output bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word,
                   output bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index,
                   output bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag,
                   output bit [ADDRESS_WIDTH-1:0] address,
                   output bit mem_write,
                   output bit mem_read,
                   output bit hit,
                   output byte unsigned byte_data_out_from_cache                    );
// UVMF_CHANGE_ME : Implement protocol monitoring.
// Reference code;
//    while (control_signal == 1'b1) @(posedge clk_i);
//    xyz = byte_to_write_i;  //    [WORD_WIDTH-1:0] 
//    xyz = address_i;  //    [ADDRESS_WIDTH-1:0] 
//    xyz = mem_write_i;  //     
//    xyz = mem_read_i;  //     
//    xyz = hit_i;  //     
//    xyz = byte_data_out_from_cache_i;  //    [WORD_WIDTH-1:0] 

      //TODO: CHANGED
      //-start_time = $time; //TODO: Esto es para algo?
      address = address_i;
      hit = hit_i;
      mem_read = mem_read_i;
      mem_write = mem_write_i;
      byte_to_write = byte_to_write_i;
      byte_data_out_from_cache = byte_data_out_from_cache_i;
      //-end_time = $time; //TODO: Esto es para algo?             

     endtask         
  
endinterface
