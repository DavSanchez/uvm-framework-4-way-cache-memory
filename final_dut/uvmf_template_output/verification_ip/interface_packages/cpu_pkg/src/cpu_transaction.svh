//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface Transaction
// File            : cpu_transaction.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class defines the variables required for an cpu
//    transaction.  Class variables to be displayed in waveform transaction
//    viewing are added to the transaction viewing stream in the add_to_wave
//    function.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_transaction       #(
      int WORD_WIDTH = 8,                                
      int ADDRESS_WIDTH = 32,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_transaction_base;

  `uvm_object_param_utils( cpu_transaction #(
                           WORD_WIDTH,
                           ADDRESS_WIDTH,
                           ADDR_WORD_FIELD_WIDTH,
                           ADDR_INDEX_FIELD_WIDTH,
                           ADDR_TAG_FIELD_WIDTH
                            ))

  rand byte unsigned byte_to_write;
  rand bit [ADDR_WORD_FIELD_WIDTH-1:0] address_word;
  rand bit [ADDR_INDEX_FIELD_WIDTH-1:0] address_index;
  rand bit [ADDR_TAG_FIELD_WIDTH-1:0] address_tag;
  bit [ADDRESS_WIDTH-1:0] address;
  rand bit mem_write;
  rand bit mem_read;
  bit hit;
  byte unsigned byte_data_out_from_cache;

//Constraints for the transaction variables:



// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "" );
    super.new( name );
 
  endfunction

// ****************************************************************************
// FUNCTION: convert2string()
// This function converts all variables in this class to a single string for 
// logfile reporting.
//
  virtual function string convert2string();
    // UVMF_CHANGE_ME : Customize format if desired.
    return $sformatf("byte_to_write:0x%x address_word:0x%x address_index:0x%x address_tag:0x%x address:0x%x mem_write:0x%x mem_read:0x%x hit:0x%x byte_data_out_from_cache:0x%x ",byte_to_write,address_word,address_index,address_tag,address,mem_write,mem_read,hit,byte_data_out_from_cache);
  endfunction

//*******************************************************************
// FUNCTION: do_print()
// This function is automatically called when the .print() function
// is called on this class.
//
  virtual function void do_print(uvm_printer printer);
    if (printer.knobs.sprint==0)
      $display(convert2string());
    else
      printer.m_string = convert2string();
  endfunction

//*******************************************************************
// FUNCTION: do_compare()
// This function is automatically called when the .compare() function
// is called on this class.
//
  virtual function bit do_compare (uvm_object rhs, uvm_comparer comparer);
    cpu_transaction   #(
            .WORD_WIDTH(WORD_WIDTH),
            .ADDRESS_WIDTH(ADDRESS_WIDTH),
            .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),
            .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),
            .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)
             ) RHS;
    if (!$cast(RHS,rhs)) return 0;
// UVMF_CHANGE_ME : Eliminate comparison of variables not to be used for compare
    return (super.do_compare(rhs,comparer)
            &&(this.byte_to_write == RHS.byte_to_write)
            &&(this.address == RHS.address)
            &&(this.mem_write == RHS.mem_write)
            &&(this.mem_read == RHS.mem_read)
            &&(this.hit == RHS.hit)
            &&(this.byte_data_out_from_cache == RHS.byte_data_out_from_cache)
            );
  endfunction

// ****************************************************************************
// FUNCTION: add_to_wave()
// This function is used to display variables in this class in the waveform 
// viewer.  The start_time and end_time variables must be set before this 
// function is called.  If the start_time and end_time variables are not set
// the transaction will be hidden at 0ns on the waveform display.
// 
  virtual function void add_to_wave(int transaction_viewing_stream_h);
    if (transaction_view_h == 0)
      transaction_view_h = $begin_transaction(transaction_viewing_stream_h,"cpu_transaction",start_time);
    // case()
    //   1 : $add_color(transaction_view_h,"red");
    //   default : $add_color(transaction_view_h,"grey");
    // endcase

    // TODO: CHANGED. Added colors to read and write transactions from CPU
    if (mem_read == 1'b1 && mem_write == 1'b0) begin
      $add_color(transaction_view_h,"green");
    end else if (mem_read == 1'b0 && mem_write == 1'b1) begin
      $add_color(transaction_view_h,"orange");
    end

    super.add_to_wave(transaction_view_h);
// UVMF_CHANGE_ME : Eliminate transaction variables not wanted in transaction viewing in the waveform viewer
    $add_attribute(transaction_view_h,byte_to_write,"byte_to_write");
    $add_attribute(transaction_view_h,address_word,"address_word");
    $add_attribute(transaction_view_h,address_index,"address_index");
    $add_attribute(transaction_view_h,address_tag,"address_tag");
    $add_attribute(transaction_view_h,address,"address");
    $add_attribute(transaction_view_h,mem_write,"mem_write");
    $add_attribute(transaction_view_h,mem_read,"mem_read");
    $add_attribute(transaction_view_h,hit,"hit");
    $add_attribute(transaction_view_h,byte_data_out_from_cache,"byte_data_out_from_cache");
    $end_transaction(transaction_view_h,end_time);
    $free_transaction(transaction_view_h);
  endfunction

endclass
