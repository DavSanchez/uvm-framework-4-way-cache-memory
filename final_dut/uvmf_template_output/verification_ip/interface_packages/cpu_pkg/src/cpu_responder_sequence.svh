//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu responder sequence
// Unit            : Interface UVM Responder
// File            : cpu_responder_sequence.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class can be used to provide stimulus when an interface
//              has been configured to run in a responder mode. It
//              will never finish by default, always going back to the driver
//              and driver BFM for the next transaction with which to respond.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class cpu_responder_sequence  #(
      int WORD_WIDTH = 8,      int ADDRESS_WIDTH = 32,      int ADDR_WORD_FIELD_WIDTH = 4,      int ADDR_INDEX_FIELD_WIDTH = 4,      int ADDR_TAG_FIELD_WIDTH = 24     )
extends cpu_sequence_base  #(
          .WORD_WIDTH(WORD_WIDTH),          .ADDRESS_WIDTH(ADDRESS_WIDTH),          .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),          .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),          .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)       ) ;

  `uvm_object_param_utils( cpu_responder_sequence #(    WORD_WIDTH,      ADDRESS_WIDTH,      ADDR_WORD_FIELD_WIDTH,      ADDR_INDEX_FIELD_WIDTH,      ADDR_TAG_FIELD_WIDTH          ))

  function new(string name = "cpu_responder_sequence");
    super.new(name);
  endfunction

  task body();
    req=cpu_transaction #(      .WORD_WIDTH(WORD_WIDTH),         .ADDRESS_WIDTH(ADDRESS_WIDTH),         .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),         .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),         .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)            ) ::type_id::create("req");
    forever begin
      start_item(req);
      finish_item(req);
      // UVMF_CHANGE_ME : Do something here with the resulting req item.  The
      // finish_item() call above will block until the req transaction is ready
      // to be handled by the responder sequence.
      // If this was an item that required a response, the expectation is
      // that the response should be populated within this transaction now.
      `uvm_info("SEQ",$sformatf("Processed txn: %s",req.convert2string()),UVM_HIGH)
    end
  endtask

endclass
