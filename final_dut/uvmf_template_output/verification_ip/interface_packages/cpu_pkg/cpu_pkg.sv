//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface HVL package
// File            : cpu_pkg.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    interface package that will run on the host simulator.
//
// CONTAINS:
//    - <cpu_typedefs_hdl>
//    - <cpu_typedefs.svh>
//    - <cpu_transaction.svh>

//    - <cpu_configuration.svh>
//    - <cpu_driver.svh>
//    - <cpu_monitor.svh>

//    - <cpu_transaction_coverage.svh>
//    - <cpu_sequence_base.svh>
//    - <cpu_random_sequence.svh>

//    - <cpu_responder_sequence.svh>
//    - <cpu2reg_adapter.svh>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package cpu_pkg;
  
   import uvm_pkg::*;
   import questa_uvm_pkg::*;
   import uvmf_base_pkg_hdl::*;
   import uvmf_base_pkg::*;
   import cpu_pkg_hdl::*;
   `include "uvm_macros.svh"
   
   export cpu_pkg_hdl::*;
   
 
   `include "src/cpu_typedefs.svh"
   `include "src/cpu_transaction.svh"

   `include "src/cpu_configuration.svh"
   `include "src/cpu_driver.svh"
   `include "src/cpu_monitor.svh"

   `include "src/cpu_transaction_coverage.svh"
   `include "src/cpu_sequence_base.svh"
   `include "src/cpu_random_sequence.svh"

   `include "src/cpu_read_sequence.svh"   // CHANGED: ADDED READ SEQUENCE
   `include "src/cpu_write_sequence.svh"  // CHANGED: ADDED WRITE SEQUENCE

   `include "src/cpu_read_burst_sequence.svh"   // CHANGED: ADDED READ SET SEQUENCE
   `include "src/cpu_write_burst_sequence.svh"  // CHANGED: ADDED WRITE SET SEQUENCE

   `include "src/cpu_responder_sequence.svh"
   `include "src/cpu2reg_adapter.svh"

   `include "src/cpu_agent.svh"

   typedef uvm_reg_predictor #(cpu_transaction) cpu2reg_predictor;


endpackage

