//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cpu interface agent
// Unit            : Interface HDL Package
// File            : cpu_pkg_hdl.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    interface package that needs to be compiled and synthesized
//    for running on Veloce.
//
// CONTAINS:
//    - <cpu_typedefs_hdl>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package cpu_pkg_hdl;
  
  import uvmf_base_pkg_hdl::*;

  `include "src/cpu_typedefs_hdl.svh"

endpackage

