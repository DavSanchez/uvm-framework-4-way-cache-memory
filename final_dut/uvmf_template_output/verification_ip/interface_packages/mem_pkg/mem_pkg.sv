//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface HVL package
// File            : mem_pkg.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    interface package that will run on the host simulator.
//
// CONTAINS:
//    - <mem_typedefs_hdl>
//    - <mem_typedefs.svh>
//    - <mem_transaction.svh>

//    - <mem_configuration.svh>
//    - <mem_driver.svh>
//    - <mem_monitor.svh>

//    - <mem_transaction_coverage.svh>
//    - <mem_sequence_base.svh>
//    - <mem_random_sequence.svh>

//    - <mem_responder_sequence.svh>
//    - <mem2reg_adapter.svh>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package mem_pkg;
  
   import uvm_pkg::*;
   import questa_uvm_pkg::*;
   import uvmf_base_pkg_hdl::*;
   import uvmf_base_pkg::*;
   import mem_pkg_hdl::*;
   `include "uvm_macros.svh"
   
   export mem_pkg_hdl::*;
   
 
   `include "src/mem_typedefs.svh"
   `include "src/mem_transaction.svh"

   `include "src/mem_configuration.svh"
   `include "src/mem_driver.svh"
   `include "src/mem_monitor.svh"

   `include "src/mem_transaction_coverage.svh"
   `include "src/mem_sequence_base.svh"
   `include "src/mem_random_sequence.svh"

   `include "src/mem_responder_sequence.svh"
   `include "src/mem2reg_adapter.svh"

   `include "src/mem_agent.svh"

   typedef uvm_reg_predictor #(mem_transaction) mem2reg_predictor;


endpackage

