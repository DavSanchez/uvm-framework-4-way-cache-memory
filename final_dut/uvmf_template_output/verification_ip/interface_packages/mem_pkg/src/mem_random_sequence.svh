//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface random sequence
// File            : mem_random_sequence.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
// This sequences randomizes the mem transaction and sends it 
// to the UVM driver.
//
// ****************************************************************************
// This sequence constructs and randomizes a mem_transaction.
// 
class mem_random_sequence  #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
extends mem_sequence_base  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ;

  `uvm_object_param_utils( mem_random_sequence #(
                           ADDRESS_WIDTH,
                           BLOCK_WIDTH,
                           WAY_SEL_WIDTH,
                           WORD_WIDTH,
                           ADDR_WORD_FIELD_WIDTH,
                           ADDR_INDEX_FIELD_WIDTH,
                           ADDR_TAG_FIELD_WIDTH
                            ))

//*****************************************************************
  function new(string name = "");
    super.new(name);
  endfunction: new

// ****************************************************************************
// TASK : body()
// This task is automatically executed when this sequence is started using the 
// start(sequencerHandle) task.
//
  task body();

    begin
      // Construct the transaction
      req=mem_transaction #( 
                .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                .BLOCK_WIDTH(BLOCK_WIDTH),                                
                .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                .WORD_WIDTH(WORD_WIDTH),                                
                .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
               ) ::type_id::create("req");

      start_item(req);
      // Randomize the transaction
      if(!req.randomize()) `uvm_fatal("SEQ", "mem_random_sequence::body()-mem_transaction randomization failed")
      // Send the transaction to the mem_driver_bfm via the sequencer and mem_driver.
      finish_item(req);
      `uvm_info("SEQ", {"Response:",req.convert2string()},UVM_MEDIUM)
    end

  endtask: body

endclass: mem_random_sequence
//----------------------------------------------------------------------
//
