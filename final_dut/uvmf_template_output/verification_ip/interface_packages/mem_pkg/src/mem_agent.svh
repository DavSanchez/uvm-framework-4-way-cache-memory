//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface UVM agent
// File            : mem_agent.svh
//----------------------------------------------------------------------
//     
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_agent #( int ADDRESS_WIDTH = 32, int BLOCK_WIDTH = 128, int WAY_SEL_WIDTH = 2, int WORD_WIDTH = 8, int ADDR_WORD_FIELD_WIDTH = 4, int ADDR_INDEX_FIELD_WIDTH = 4, int ADDR_TAG_FIELD_WIDTH = 24)extends uvmf_parameterized_agent #(
                    .CONFIG_T(mem_configuration #(.ADDRESS_WIDTH(ADDRESS_WIDTH),.BLOCK_WIDTH(BLOCK_WIDTH),.WAY_SEL_WIDTH(WAY_SEL_WIDTH),.WORD_WIDTH(WORD_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .DRIVER_T(mem_driver #(.ADDRESS_WIDTH(ADDRESS_WIDTH),.BLOCK_WIDTH(BLOCK_WIDTH),.WAY_SEL_WIDTH(WAY_SEL_WIDTH),.WORD_WIDTH(WORD_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .MONITOR_T(mem_monitor #(.ADDRESS_WIDTH(ADDRESS_WIDTH),.BLOCK_WIDTH(BLOCK_WIDTH),.WAY_SEL_WIDTH(WAY_SEL_WIDTH),.WORD_WIDTH(WORD_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .COVERAGE_T(mem_transaction_coverage #(.ADDRESS_WIDTH(ADDRESS_WIDTH),.BLOCK_WIDTH(BLOCK_WIDTH),.WAY_SEL_WIDTH(WAY_SEL_WIDTH),.WORD_WIDTH(WORD_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH))),
                    .TRANS_T(mem_transaction #(.ADDRESS_WIDTH(ADDRESS_WIDTH),.BLOCK_WIDTH(BLOCK_WIDTH),.WAY_SEL_WIDTH(WAY_SEL_WIDTH),.WORD_WIDTH(WORD_WIDTH),.ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),.ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),.ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)))
                    );

  `uvm_component_param_utils (mem_agent #(ADDRESS_WIDTH,BLOCK_WIDTH,WAY_SEL_WIDTH,WORD_WIDTH,ADDR_WORD_FIELD_WIDTH,ADDR_INDEX_FIELD_WIDTH,ADDR_TAG_FIELD_WIDTH) )

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

endclass
