//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface UVM Driver
// File            : mem_driver.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class passes transactions between the sequencer
//        and the BFM driver interface.  It accesses the driver BFM 
//        through the bfm handle. This driver
//        passes transactions to the driver BFM through the access
//        task.  
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_driver  #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_driver_base #(
                   .CONFIG_T(mem_configuration  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .BFM_BIND_T(virtual mem_driver_bfm #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .REQ(mem_transaction  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                   .RSP(mem_transaction  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ));

  `uvm_component_param_utils( mem_driver #(
                              ADDRESS_WIDTH,
                              BLOCK_WIDTH,
                              WAY_SEL_WIDTH,
                              WORD_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent=null );
    super.new( name, parent );
  endfunction

// ****************************************************************************
   virtual function void configure(input CONFIG_T cfg);
      bfm.configure(

          cfg.active_passive,
          cfg.initiator_responder
);                    
   
   endfunction

// ****************************************************************************
   virtual function void set_bfm_proxy_handle();
      bfm.proxy = this;
   endfunction

// ****************************************************************************              
  virtual task access( inout REQ txn );
      if (configuration.initiator_responder==RESPONDER) begin
        if (txn.mem_read != txn.mem_write) begin
          bfm.do_response_ready(
             txn.input_block                      );
        end
        bfm.response(
      txn.address,
      txn.mem_write,
      txn.mem_read,
      txn.input_block,
      txn.replacement_block,
      txn.write_back,
      txn.valid,
      txn.dirty,
      txn.way_sel,
      txn.tag_out        
          );
      end else begin    
        bfm.access(
      txn.address,
      txn.mem_write,
      txn.mem_read,
      txn.input_block,
      txn.replacement_block,
      txn.write_back,
      txn.valid,
      txn.dirty,
      txn.way_sel,
      txn.tag_out            );
    end
  endtask

endclass
