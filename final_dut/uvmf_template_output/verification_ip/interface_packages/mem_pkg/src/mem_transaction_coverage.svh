//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface Transaction Coverage
// File            : mem_transaction_coverage.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class records mem transaction information using
//       a covergroup named mem_transaction_cg.  An instance of this
//       coverage component is instantiated in the uvmf_parameterized_agent
//       if the has_coverage flag is set.
//
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_transaction_coverage  #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvm_subscriber #(.T(mem_transaction  #(
                                            .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                                            .BLOCK_WIDTH(BLOCK_WIDTH),                                
                                            .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                                            .WORD_WIDTH(WORD_WIDTH),                                
                                            .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                                            .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                                            .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                                            ) ));

  `uvm_component_param_utils( mem_transaction_coverage #(
                              ADDRESS_WIDTH,
                              BLOCK_WIDTH,
                              WAY_SEL_WIDTH,
                              WORD_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

  bit [ADDRESS_WIDTH-1:0] address;
  bit mem_write;
  bit mem_read;
  bit [BLOCK_WIDTH-1:0] input_block;
  bit [BLOCK_WIDTH-1:0] replacement_block;
  bit write_back;
  bit valid;
  bit dirty;
  bit [WAY_SEL_WIDTH-1:0] way_sel;
  bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out;

// ****************************************************************************
  // UVMF_CHANGE_ME : Add coverage bins, crosses, exclusions, etc. according to coverage needs.
  covergroup mem_transaction_cg;
    option.auto_bin_max=1024;
    coverpoint address;
    coverpoint mem_write;
    coverpoint mem_read;
    coverpoint input_block;
    coverpoint replacement_block;
    coverpoint write_back;
    coverpoint valid;
    coverpoint dirty;
    coverpoint way_sel;
    coverpoint tag_out;
  endgroup

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new(string name="", uvm_component parent=null);
    super.new(name,parent);
    mem_transaction_cg=new;
    mem_transaction_cg.set_inst_name($sformatf("mem_transaction_cg_%s",get_full_name()));
 endfunction

// ****************************************************************************
// FUNCTION: write (T t)
// This function is automatically executed when a transaction arrives on the
// analysis_export.  It copies values from the variables in the transaction 
// to local variables used to collect functional coverage.  
//
  virtual function void write (T t);
    `uvm_info("COV","Received transaction",UVM_HIGH);
    address = t.address;
    mem_write = t.mem_write;
    mem_read = t.mem_read;
    input_block = t.input_block;
    replacement_block = t.replacement_block;
    write_back = t.write_back;
    valid = t.valid;
    dirty = t.dirty;
    way_sel = t.way_sel;
    tag_out = t.tag_out;
    mem_transaction_cg.sample();
  endfunction

endclass
