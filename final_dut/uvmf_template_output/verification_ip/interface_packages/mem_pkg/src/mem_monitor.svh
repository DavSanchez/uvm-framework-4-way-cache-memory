//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface UVM monitor
// File            : mem_monitor.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class receives mem transactions observed by the
//     mem monitor BFM and broadcasts them through the analysis port
//     on the agent. It accesses the monitor BFM through the monitor
//     task. This UVM component captures transactions
//     for viewing in the waveform viewer if the
//     enable_transaction_viewing flag is set in the configuration.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_monitor  #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_monitor_base #(
                    .CONFIG_T(mem_configuration  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                    .BFM_BIND_T(virtual mem_monitor_bfm  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ),
                    .TRANS_T(mem_transaction  #(
                             .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
                             .BLOCK_WIDTH(BLOCK_WIDTH),                                
                             .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
                             .WORD_WIDTH(WORD_WIDTH),                                
                             .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
                             .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
                             .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                             ) ));

  `uvm_component_param_utils( mem_monitor #(
                              ADDRESS_WIDTH,
                              BLOCK_WIDTH,
                              WAY_SEL_WIDTH,
                              WORD_WIDTH,
                              ADDR_WORD_FIELD_WIDTH,
                              ADDR_INDEX_FIELD_WIDTH,
                              ADDR_TAG_FIELD_WIDTH
                            ))

// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

// ****************************************************************************
   virtual function void configure(input CONFIG_T cfg);
      bfm.configure(

          cfg.active_passive,
          cfg.initiator_responder
);                    
   
   endfunction

// ****************************************************************************
   virtual function void set_bfm_proxy_handle();
      bfm.proxy = this;
   endfunction

 // ****************************************************************************              
  virtual task run_phase(uvm_phase phase);                                                   
  // Start monitor BFM thread and don't call super.run() in order to                       
  // override the default monitor proxy 'pull' behavior with the more                      
  // emulation-friendly BFM 'push' approach using the notify_transaction                   
  // function below                                                                        
  bfm.start_monitoring();                                                   
  endtask                                                                                    
  
  // ****************************************************************************              
  virtual function void notify_transaction(
                        input bit [ADDRESS_WIDTH-1:0] address,  
                        input bit mem_write,  
                        input bit mem_read,  
                        input bit [BLOCK_WIDTH-1:0] input_block,  
                        input bit [BLOCK_WIDTH-1:0] replacement_block,  
                        input bit write_back,  
                        input bit valid,  
                        input bit dirty,  
                        input bit [WAY_SEL_WIDTH-1:0] way_sel,  
                        input bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out 
                        );
    trans = new("trans");                                                                   
    trans.start_time = time_stamp;                                                          
    trans.end_time = $time;                                                                 
    time_stamp = trans.end_time;                                                            
    trans.address = address;
    trans.mem_write = mem_write;
    trans.mem_read = mem_read;
    trans.input_block = input_block;
    trans.replacement_block = replacement_block;
    trans.write_back = write_back;
    trans.valid = valid;
    trans.dirty = dirty;
    trans.way_sel = way_sel;
    trans.tag_out = tag_out;
    analyze(trans);                                                                         
  endfunction  

endclass
