//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface Transaction
// File            : mem_transaction.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class defines the variables required for an mem
//    transaction.  Class variables to be displayed in waveform transaction
//    viewing are added to the transaction viewing stream in the add_to_wave
//    function.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_transaction       #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      ) extends uvmf_transaction_base;

  `uvm_object_param_utils( mem_transaction #(
                           ADDRESS_WIDTH,
                           BLOCK_WIDTH,
                           WAY_SEL_WIDTH,
                           WORD_WIDTH,
                           ADDR_WORD_FIELD_WIDTH,
                           ADDR_INDEX_FIELD_WIDTH,
                           ADDR_TAG_FIELD_WIDTH
                            ))

  bit [ADDRESS_WIDTH-1:0] address;
  bit mem_write;
  bit mem_read;
  rand bit [BLOCK_WIDTH-1:0] input_block;
  bit [BLOCK_WIDTH-1:0] replacement_block;
  bit write_back;
  bit valid;
  bit dirty;
  bit [WAY_SEL_WIDTH-1:0] way_sel;
  bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out;

//Constraints for the transaction variables:



// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "" );
    super.new( name );
 
  endfunction

// ****************************************************************************
// FUNCTION: convert2string()
// This function converts all variables in this class to a single string for 
// logfile reporting.
//
  virtual function string convert2string();
    // UVMF_CHANGE_ME : Customize format if desired.
    return $sformatf("address:0x%x mem_write:0x%x mem_read:0x%x input_block:0x%x replacement_block:0x%x write_back:0x%x valid:0x%x dirty:0x%x way_sel:0x%x tag_out:0x%x ",address,mem_write,mem_read,input_block,replacement_block,write_back,valid,dirty,way_sel,tag_out);
  endfunction

//*******************************************************************
// FUNCTION: do_print()
// This function is automatically called when the .print() function
// is called on this class.
//
  virtual function void do_print(uvm_printer printer);
    if (printer.knobs.sprint==0)
      $display(convert2string());
    else
      printer.m_string = convert2string();
  endfunction

//*******************************************************************
// FUNCTION: do_compare()
// This function is automatically called when the .compare() function
// is called on this class.
//
  virtual function bit do_compare (uvm_object rhs, uvm_comparer comparer);
    mem_transaction   #(
            .ADDRESS_WIDTH(ADDRESS_WIDTH),
            .BLOCK_WIDTH(BLOCK_WIDTH),
            .WAY_SEL_WIDTH(WAY_SEL_WIDTH),
            .WORD_WIDTH(WORD_WIDTH),
            .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),
            .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),
            .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)
             ) RHS;
    if (!$cast(RHS,rhs)) return 0;
// UVMF_CHANGE_ME : Eliminate comparison of variables not to be used for compare
    return (super.do_compare(rhs,comparer)
            &&(this.address == RHS.address)
            &&(this.mem_write == RHS.mem_write)
            &&(this.mem_read == RHS.mem_read)
            &&(this.input_block == RHS.input_block)
            &&(this.replacement_block == RHS.replacement_block)
            &&(this.write_back == RHS.write_back)
            &&(this.valid == RHS.valid)
            &&(this.dirty == RHS.dirty)
            &&(this.way_sel == RHS.way_sel)
            &&(this.tag_out == RHS.tag_out)
            );
  endfunction

// ****************************************************************************
// FUNCTION: add_to_wave()
// This function is used to display variables in this class in the waveform 
// viewer.  The start_time and end_time variables must be set before this 
// function is called.  If the start_time and end_time variables are not set
// the transaction will be hidden at 0ns on the waveform display.
// 
  virtual function void add_to_wave(int transaction_viewing_stream_h);
    if (transaction_view_h == 0)
      transaction_view_h = $begin_transaction(transaction_viewing_stream_h,"mem_transaction",start_time);
    // case()
    //   1 : $add_color(transaction_view_h,"red");
    //   default : $add_color(transaction_view_h,"grey");
    // endcase
      
    // TODO: CHANGED. Added colors to read and write transactions from CPU
    if (mem_read == 1'b1 && mem_write == 1'b0) begin
      $add_color(transaction_view_h,"green");
    end else if (mem_read == 1'b0 && mem_write == 1'b1) begin
      $add_color(transaction_view_h,"orange");
    end

    super.add_to_wave(transaction_view_h);
// UVMF_CHANGE_ME : Eliminate transaction variables not wanted in transaction viewing in the waveform viewer
    $add_attribute(transaction_view_h,address,"address");
    $add_attribute(transaction_view_h,mem_write,"mem_write");
    $add_attribute(transaction_view_h,mem_read,"mem_read");
    $add_attribute(transaction_view_h,input_block,"input_block");
    $add_attribute(transaction_view_h,replacement_block,"replacement_block");
    $add_attribute(transaction_view_h,write_back,"write_back");
    $add_attribute(transaction_view_h,valid,"valid");
    $add_attribute(transaction_view_h,dirty,"dirty");
    $add_attribute(transaction_view_h,way_sel,"way_sel");
    $add_attribute(transaction_view_h,tag_out,"tag_out");
    $end_transaction(transaction_view_h,end_time);
    $free_transaction(transaction_view_h);
  endfunction

endclass
