//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem responder sequence
// Unit            : Interface UVM Responder
// File            : mem_responder_sequence.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: This class can be used to provide stimulus when an interface
//              has been configured to run in a responder mode. It
//              will never finish by default, always going back to the driver
//              and driver BFM for the next transaction with which to respond.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
class mem_responder_sequence  #(
      int ADDRESS_WIDTH = 32,      int BLOCK_WIDTH = 128,      int WAY_SEL_WIDTH = 2,      int WORD_WIDTH = 8,      int ADDR_WORD_FIELD_WIDTH = 4,      int ADDR_INDEX_FIELD_WIDTH = 4,      int ADDR_TAG_FIELD_WIDTH = 24     )
extends mem_sequence_base  #(
          .ADDRESS_WIDTH(ADDRESS_WIDTH),          .BLOCK_WIDTH(BLOCK_WIDTH),          .WAY_SEL_WIDTH(WAY_SEL_WIDTH),          .WORD_WIDTH(WORD_WIDTH),          .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),          .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),          .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)       ) ;

  `uvm_object_param_utils( mem_responder_sequence #(    ADDRESS_WIDTH,      BLOCK_WIDTH,      WAY_SEL_WIDTH,      WORD_WIDTH,      ADDR_WORD_FIELD_WIDTH,      ADDR_INDEX_FIELD_WIDTH,      ADDR_TAG_FIELD_WIDTH          ))

  // TODO: CHANGED
  bit [BLOCK_WIDTH-1:0] memory [bit[ADDRESS_WIDTH-1:ADDR_WORD_FIELD_WIDTH-1]];
  rand bit [BLOCK_WIDTH-1:0] data;
  bit [ADDR_TAG_FIELD_WIDTH+ADDR_INDEX_FIELD_WIDTH-1:0] index;

  function new(string name = "mem_responder_sequence");
    super.new(name);
  endfunction

  task body();
    req=mem_transaction #(      .ADDRESS_WIDTH(ADDRESS_WIDTH),         .BLOCK_WIDTH(BLOCK_WIDTH),         .WAY_SEL_WIDTH(WAY_SEL_WIDTH),         .WORD_WIDTH(WORD_WIDTH),         .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),         .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),         .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)            ) ::type_id::create("req");
    forever begin
      start_item(req);
      finish_item(req);
      // UVMF_CHANGE_ME : Do something here with the resulting req item.  The
      // finish_item() call above will block until the req transaction is ready
      // to be handled by the responder sequence.
      // If this was an item that required a response, the expectation is
      // that the response should be populated within this transaction now.      
      
      // TODO: CHANGED
      $display("cache_mem_responder_sequence: Evaluating R/W signals...");
      $display("cache_mem_responder_sequence: mem_read value: 0x%h", req.mem_read);
      $display("cache_mem_responder_sequence: mem_write value: 0x%h", req.mem_write);
      $display("cache_mem_responder_sequence: address value: 0x%h", req.address);
      if(req.mem_read != req.mem_write) begin
        $display("Entered response condition!!");
        // Truncamos los bits menos significativos de la dirección para obviar el campo WORD
        index = req.address[ADDRESS_WIDTH-1:ADDR_WORD_FIELD_WIDTH-1];
        // Chequeamos el índice en el array asociativo, si no existe el elemento lo creamos y almacenamos
        if(!memory.exists(index)) begin
          if (!std::randomize(data)) `uvm_fatal("SEQ", "mem_responder_sequence::body()-mem_transaction randomization failed")
          // consider the insert method: memory.insert(index, data);
          memory[index] = data;
        end
        req.input_block = memory[index];
      end
      get_response(rsp);
      `uvm_info("SEQ",$sformatf("Processed txn: %s",req.convert2string()),UVM_HIGH)
    end
  endtask

endclass
