//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface Signal Bundle
// File            : mem_if.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: This interface contains the mem interface signals.
//      It is instantiated once per mem bus.  Bus Functional Models, 
//      BFM's named mem_driver_bfm, are used to drive signals on the bus.
//      BFM's named mem_monitor_bfm are used to monitor signals on the 
//      bus. This interface signal bundle is passed in the port list of
//      the BFM in order to give the BFM access to the signals in this
//      interface.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
// This template can be used to connect a DUT to these signals
//
// .dut_signal_port(mem_bus.address), // Agent output 
// .dut_signal_port(mem_bus.mem_write), // Agent output 
// .dut_signal_port(mem_bus.mem_read), // Agent output 
// .dut_signal_port(mem_bus.input_block), // Agent input 
// .dut_signal_port(mem_bus.replacement_block), // Agent output 
// .dut_signal_port(mem_bus.write_back), // Agent output 
// .dut_signal_port(mem_bus.valid), // Agent output 
// .dut_signal_port(mem_bus.dirty), // Agent output 
// .dut_signal_port(mem_bus.way_sel), // Agent output 
// .dut_signal_port(mem_bus.tag_out), // Agent output 

import uvmf_base_pkg_hdl::*;
import mem_pkg_hdl::*;

interface  mem_if       #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
 ( 
  input tri clk, 
  input tri rst
  ,inout tri [ADDRESS_WIDTH-1:0] address
  ,inout tri  mem_write
  ,inout tri  mem_read
  ,inout tri [BLOCK_WIDTH-1:0] input_block
  ,inout tri [BLOCK_WIDTH-1:0] replacement_block
  ,inout tri  write_back
  ,inout tri  valid
  ,inout tri  dirty
  ,inout tri [WAY_SEL_WIDTH-1:0] way_sel
  ,inout tri [ADDR_TAG_FIELD_WIDTH-1:0] tag_out
);

endinterface

