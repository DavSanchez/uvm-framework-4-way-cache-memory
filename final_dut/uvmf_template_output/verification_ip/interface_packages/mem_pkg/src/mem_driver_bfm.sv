//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface Driver BFM
// File            : mem_driver_bfm.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
//    This interface performs the mem signal driving.  It is
//     accessed by the uvm mem driver through a virtual interface
//     handle in the mem configuration.  It drives the singals passed
//     in through the port connection named bus of type mem_if.
//
//     Input signals from the mem_if are assigned to an internal input
//     signal with a _i suffix.  The _i signal should be used for sampling.
//
//     The input signal connections are as follows:
//       bus.signal -> signal_i 
//
//     This bfm drives signals with a _o suffix.  These signals
//     are driven onto signals within mem_if based on INITIATOR/RESPONDER and/or
//     ARBITRATION/GRANT status.  
//
//     The output signal connections are as follows:
//        signal_o -> bus.signal
//
//                                                                                           
//      Interface functions and tasks used by UVM components:                                
//             configure(uvmf_initiator_responder_t mst_slv);                                       
//                   This function gets configuration attributes from the                    
//                   UVM driver to set any required BFM configuration                        
//                   variables such as 'initiator_responder'.                                       
//                                                                                           
//             access(
//       bit [ADDRESS_WIDTH-1:0] address,
//       bit mem_write,
//       bit mem_read,
//       bit [BLOCK_WIDTH-1:0] input_block,
//       bit [BLOCK_WIDTH-1:0] replacement_block,
//       bit write_back,
//       bit valid,
//       bit dirty,
//       bit [WAY_SEL_WIDTH-1:0] way_sel,
//       bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out );//                   );
//                   This task receives transaction attributes from the                      
//                   UVM driver and then executes the corresponding                          
//                   bus operation on the bus. 
//
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
import uvmf_base_pkg_hdl::*;
import mem_pkg_hdl::*;

interface mem_driver_bfm       #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
(mem_if  bus);
// pragma attribute mem_driver_bfm partition_interface_xif
// The above pragma and additional ones in-lined below are for running this BFM on Veloce

  // Config value to determine if this is an initiator or a responder 
  uvmf_initiator_responder_t initiator_responder;

  tri        clk_i;
  tri        rst_i;

// Signal list (all signals are capable of being inputs and outputs for the sake
// of supporting both INITIATOR and RESPONDER mode operation. Expectation is that 
// directionality in the config file was from the point-of-view of the INITIATOR

// INITIATOR mode input signals
  tri       [BLOCK_WIDTH-1:0]  input_block_i;
  bit       [BLOCK_WIDTH-1:0]  input_block_o;

// INITIATOR mode output signals
  tri       [ADDRESS_WIDTH-1:0]  address_i;
  bit       [ADDRESS_WIDTH-1:0]  address_o;
  tri         mem_write_i;
  bit         mem_write_o;
  tri         mem_read_i;
  bit         mem_read_o;
  tri       [BLOCK_WIDTH-1:0]  replacement_block_i;
  bit       [BLOCK_WIDTH-1:0]  replacement_block_o;
  tri         write_back_i;
  bit         write_back_o;
  tri         valid_i;
  bit         valid_o;
  tri         dirty_i;
  bit         dirty_o;
  tri       [WAY_SEL_WIDTH-1:0]  way_sel_i;
  bit       [WAY_SEL_WIDTH-1:0]  way_sel_o;
  tri       [ADDR_TAG_FIELD_WIDTH-1:0]  tag_out_i;
  bit       [ADDR_TAG_FIELD_WIDTH-1:0]  tag_out_o;

// Bi-directional signals
  

  assign     clk_i    =   bus.clk;
  assign     rst_i    =   bus.rst;

  // These are signals marked as 'input' by the config file, but the signals will be
  // driven by this BFM if put into RESPONDER mode (flipping all signal directions around)
  assign     input_block_i = bus.input_block;
  assign     bus.input_block = (initiator_responder == RESPONDER) ? input_block_o : 'bz;


  // These are signals marked as 'output' by the config file, but the outputs will
  // not be driven by this BFM unless placed in INITIATOR mode.
  assign bus.address = (initiator_responder == INITIATOR) ? address_o : 'bz;
  assign address_i = bus.address;
  assign bus.mem_write = (initiator_responder == INITIATOR) ? mem_write_o : 'bz;
  assign mem_write_i = bus.mem_write;
  assign bus.mem_read = (initiator_responder == INITIATOR) ? mem_read_o : 'bz;
  assign mem_read_i = bus.mem_read;
  assign bus.replacement_block = (initiator_responder == INITIATOR) ? replacement_block_o : 'bz;
  assign replacement_block_i = bus.replacement_block;
  assign bus.write_back = (initiator_responder == INITIATOR) ? write_back_o : 'bz;
  assign write_back_i = bus.write_back;
  assign bus.valid = (initiator_responder == INITIATOR) ? valid_o : 'bz;
  assign valid_i = bus.valid;
  assign bus.dirty = (initiator_responder == INITIATOR) ? dirty_o : 'bz;
  assign dirty_i = bus.dirty;
  assign bus.way_sel = (initiator_responder == INITIATOR) ? way_sel_o : 'bz;
  assign way_sel_i = bus.way_sel;
  assign bus.tag_out = (initiator_responder == INITIATOR) ? tag_out_o : 'bz;
  assign tag_out_i = bus.tag_out;

   // Proxy handle to UVM driver
   mem_pkg::mem_driver  #(
              .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
              .BLOCK_WIDTH(BLOCK_WIDTH),                                
              .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
              .WORD_WIDTH(WORD_WIDTH),                                
              .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
              .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
              .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                    )  proxy;
  // pragma tbx oneway proxy.my_function_name_in_uvm_driver                 

//******************************************************************                         
   function void configure(
          uvmf_active_passive_t active_passive,
          uvmf_initiator_responder_t   init_resp
); // pragma tbx xtf                   
      initiator_responder = init_resp;
   
   endfunction                                                                               


// ****************************************************************************
  task do_transfer(                input bit [ADDRESS_WIDTH-1:0] address,
                input bit mem_write,
                input bit mem_read,
                input bit [BLOCK_WIDTH-1:0] input_block,
                input bit [BLOCK_WIDTH-1:0] replacement_block,
                input bit write_back,
                input bit valid,
                input bit dirty,
                input bit [WAY_SEL_WIDTH-1:0] way_sel,
                input bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out               );                                                  
  // UVMF_CHANGE_ME : Implement protocol signaling.
  // Transfers are protocol specific and therefore not generated by the templates.
  // Use the following as examples of transferring data between a sequence and the bus
  // In the wb_pkg - wb_master_access_sequence.svh, wb_driver_bfm.sv
  // Reference code;
  //    while (control_signal == 1'b1) @(posedge clk_i);
  //    INITIATOR mode input signals
  //    input_block_i;        //   [BLOCK_WIDTH-1:0] 
  //    input_block_o <= xyz; //   [BLOCK_WIDTH-1:0]  
  //    INITIATOR mode output signals
  //    address_i;        //   [ADDRESS_WIDTH-1:0]  
  //    address_o <= xyz; //   [ADDRESS_WIDTH-1:0]  
  //    mem_write_i;        //     
  //    mem_write_o <= xyz; //     
  //    mem_read_i;        //     
  //    mem_read_o <= xyz; //     
  //    replacement_block_i;        //   [BLOCK_WIDTH-1:0]  
  //    replacement_block_o <= xyz; //   [BLOCK_WIDTH-1:0]  
  //    write_back_i;        //     
  //    write_back_o <= xyz; //     
  //    valid_i;        //     
  //    valid_o <= xyz; //     
  //    dirty_i;        //     
  //    dirty_o <= xyz; //     
  //    way_sel_i;        //   [WAY_SEL_WIDTH-1:0]  
  //    way_sel_o <= xyz; //   [WAY_SEL_WIDTH-1:0]  
  //    tag_out_i;        //   [ADDR_TAG_FIELD_WIDTH-1:0]  
  //    tag_out_o <= xyz; //   [ADDR_TAG_FIELD_WIDTH-1:0]  
  //    Bi-directional signals

  $display("mem_driver_bfm: Inside do_transfer()");
endtask        

  // UVMF_CHANGE_ME : Implement response protocol signaling.
  // Templates also do not generate protocol specific response signaling. Use the 
  // following as examples for transferring data between a sequence and the bus
  // In wb_pkg - wb_memory_slave_sequence.svh, wb_driver_bfm.sv

  task do_response(                 output bit [ADDRESS_WIDTH-1:0] address,
                 output bit mem_write,
                 output bit mem_read,
                 output bit [BLOCK_WIDTH-1:0] input_block,
                 output bit [BLOCK_WIDTH-1:0] replacement_block,
                 output bit write_back,
                 output bit valid,
                 output bit dirty,
                 output bit [WAY_SEL_WIDTH-1:0] way_sel,
                 output bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out       );

    // TODO: CHANGED
    //input_block_o <= input_block;
    address = address_i;
    mem_read = mem_read_i;
    mem_write = mem_write_i;

  endtask

  // The resp_ready bit is intended to act as a simple event scheduler and does
  // not have anything to do with the protocol. It is intended to be set by
  // a proxy call to do_response_ready() and ultimately cleared somewhere within the always
  // block below.  In this simple situation, resp_ready will be cleared on the
  // clock cycle immediately following it being set.  In a more complex protocol,
  // the resp_ready signal could be an input to an explicit FSM to properly
  // time the responses to transactions.  
  bit resp_ready;
  always @(posedge clk_i) begin
    if (resp_ready) begin
      resp_ready <= 1'b0;
    end
  end

  function void do_response_ready(      input bit [BLOCK_WIDTH-1:0] input_block      );  // pragma tbx xtf
    // UVMF_CHANGE_ME : Implement response - drive BFM outputs based on the arguments
    // passed into this function.  IMPORTANT - Must not consume time (it must remain
    // a function)
    input_block_o <= input_block;
    resp_ready <= 1'b1;
  endfunction

// ****************************************************************************              
// UVMF_CHANGE_ME : Note that all transaction variables are passed into the access
//   task as inputs.  Some of these may need to be changed to outputs based on
//   protocol needs.
//
  task access(
    input   bit [ADDRESS_WIDTH-1:0] address,
    input   bit mem_write,
    input   bit mem_read,
    input   bit [BLOCK_WIDTH-1:0] input_block,
    input   bit [BLOCK_WIDTH-1:0] replacement_block,
    input   bit write_back,
    input   bit valid,
    input   bit dirty,
    input   bit [WAY_SEL_WIDTH-1:0] way_sel,
    input   bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out );
  // pragma tbx xtf                    
  @(posedge clk_i);                                                                     
  $display("mem_driver_bfm: Inside access()");
  do_transfer(
    address,
    mem_write,
    mem_read,
    input_block,
    replacement_block,
    write_back,
    valid,
    dirty,
    way_sel,
    tag_out          );                                                  
  endtask      

// ****************************************************************************              
// UVMF_CHANGE_ME : Note that all transaction variables are passed into the response
//   task as outputs.  Some of these may need to be changed to inputs based on
//   protocol needs.
  task response(
 output bit [ADDRESS_WIDTH-1:0] address,
 output bit mem_write,
 output bit mem_read,
 input bit [BLOCK_WIDTH-1:0] input_block,
 output bit [BLOCK_WIDTH-1:0] replacement_block,
 output bit write_back,
 output bit valid,
 output bit dirty,
 output bit [WAY_SEL_WIDTH-1:0] way_sel,
 output bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out );
  // pragma tbx xtf
     @(posedge clk_i);
     $display("mem_driver_bfm: Inside response()");
    do_response(
      address,
      mem_write,
      mem_read,
      input_block,
      replacement_block,
      write_back,
      valid,
      dirty,
      way_sel,
      tag_out        );
  endtask             
  
endinterface
