//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : mem interface agent
// Unit            : Interface Monitor BFM
// File            : mem_monitor_bfm.sv
//----------------------------------------------------------------------
//     
// DESCRIPTION: This interface performs the mem signal monitoring.
//      It is accessed by the uvm mem monitor through a virtual
//      interface handle in the mem configuration.  It monitors the
//      signals passed in through the port connection named bus of
//      type mem_if.
//
//     Input signals from the mem_if are assigned to an internal input
//     signal with a _i suffix.  The _i signal should be used for sampling.
//
//     The input signal connections are as follows:
//       bus.signal -> signal_i 
//
//      Interface functions and tasks used by UVM components:
//             monitor(inout TRANS_T txn);
//                   This task receives the transaction, txn, from the
//                   UVM monitor and then populates variables in txn
//                   from values observed on bus activity.  This task
//                   blocks until an operation on the mem bus is complete.
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
import uvmf_base_pkg_hdl::*;
import mem_pkg_hdl::*;

interface mem_monitor_bfm       #(
      int ADDRESS_WIDTH = 32,                                
      int BLOCK_WIDTH = 128,                                
      int WAY_SEL_WIDTH = 2,                                
      int WORD_WIDTH = 8,                                
      int ADDR_WORD_FIELD_WIDTH = 4,                                
      int ADDR_INDEX_FIELD_WIDTH = 4,                                
      int ADDR_TAG_FIELD_WIDTH = 24                                
      )
( mem_if  bus );
// pragma attribute mem_monitor_bfm partition_interface_xif                                  
// The above pragma and additional ones in-lined below are for running this BFM on Veloce


   tri        clk_i;
   tri        rst_i;
   tri        [ADDRESS_WIDTH-1:0] address_i;
   tri         mem_write_i;
   tri         mem_read_i;
   tri        [BLOCK_WIDTH-1:0] input_block_i;
   tri        [BLOCK_WIDTH-1:0] replacement_block_i;
   tri         write_back_i;
   tri         valid_i;
   tri         dirty_i;
   tri        [WAY_SEL_WIDTH-1:0] way_sel_i;
   tri        [ADDR_TAG_FIELD_WIDTH-1:0] tag_out_i;

   assign     clk_i    =   bus.clk;
   assign     rst_i    =   bus.rst;
   assign     address_i = bus.address;
   assign     mem_write_i = bus.mem_write;
   assign     mem_read_i = bus.mem_read;
   assign     input_block_i = bus.input_block;
   assign     replacement_block_i = bus.replacement_block;
   assign     write_back_i = bus.write_back;
   assign     valid_i = bus.valid;
   assign     dirty_i = bus.dirty;
   assign     way_sel_i = bus.way_sel;
   assign     tag_out_i = bus.tag_out;

   // Proxy handle to UVM monitor
   mem_pkg::mem_monitor  #(
              .ADDRESS_WIDTH(ADDRESS_WIDTH),                                
              .BLOCK_WIDTH(BLOCK_WIDTH),                                
              .WAY_SEL_WIDTH(WAY_SEL_WIDTH),                                
              .WORD_WIDTH(WORD_WIDTH),                                
              .ADDR_WORD_FIELD_WIDTH(ADDR_WORD_FIELD_WIDTH),                                
              .ADDR_INDEX_FIELD_WIDTH(ADDR_INDEX_FIELD_WIDTH),                                
              .ADDR_TAG_FIELD_WIDTH(ADDR_TAG_FIELD_WIDTH)                                
                    )  proxy;
  // pragma tbx oneway proxy.notify_transaction                 

//******************************************************************                         
   task wait_for_reset(); // pragma tbx xtf                                                  
      @(posedge clk_i) ;                                                                    
      do_wait_for_reset();                                                                   
   endtask                                                                                   

// ****************************************************************************              
   task do_wait_for_reset();                                                                 
      wait ( rst_i == 0 ) ;                                                              
      @(posedge clk_i) ;                                                                    
   endtask    
   
//******************************************************************                         
   task wait_for_num_clocks( input int unsigned count); // pragma tbx xtf                           
      @(posedge clk_i);                                                                     
      repeat (count-1) @(posedge clk_i);                                                    
   endtask      

//******************************************************************                         
  event go;                                                                                 
  function void start_monitoring(); // pragma tbx xtf      
     -> go;                                                                                 
  endfunction                                                                               
  
  // ****************************************************************************              
  initial begin                                                                             
     @go;                                                                                   
     forever begin                                                                          
        bit [ADDRESS_WIDTH-1:0] address;
        bit mem_write;
        bit mem_read;
        bit [BLOCK_WIDTH-1:0] input_block;
        bit [BLOCK_WIDTH-1:0] replacement_block;
        bit write_back;
        bit valid;
        bit dirty;
        bit [WAY_SEL_WIDTH-1:0] way_sel;
        bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out;
        @(posedge clk_i);                                                                   

        do_monitor(
                   address,
                   mem_write,
                   mem_read,
                   input_block,
                   replacement_block,
                   write_back,
                   valid,
                   dirty,
                   way_sel,
                   tag_out                  );
        proxy.notify_transaction(
                   address,
                   mem_write,
                   mem_read,
                   input_block,
                   replacement_block,
                   write_back,
                   valid,
                   dirty,
                   way_sel,
                   tag_out                                );     
     end                                                                                    
  end                                                                                       

//******************************************************************
   function void configure(
          uvmf_active_passive_t active_passive,
          uvmf_initiator_responder_t   initiator_responder
); // pragma tbx xtf

   endfunction


// ****************************************************************************              
     task do_monitor(
                   output bit [ADDRESS_WIDTH-1:0] address,
                   output bit mem_write,
                   output bit mem_read,
                   output bit [BLOCK_WIDTH-1:0] input_block,
                   output bit [BLOCK_WIDTH-1:0] replacement_block,
                   output bit write_back,
                   output bit valid,
                   output bit dirty,
                   output bit [WAY_SEL_WIDTH-1:0] way_sel,
                   output bit [ADDR_TAG_FIELD_WIDTH-1:0] tag_out                    );
// UVMF_CHANGE_ME : Implement protocol monitoring.
// Reference code;
//    while (control_signal == 1'b1) @(posedge clk_i);
//    xyz = address_i;  //    [ADDRESS_WIDTH-1:0] 
//    xyz = mem_write_i;  //     
//    xyz = mem_read_i;  //     
//    xyz = input_block_i;  //    [BLOCK_WIDTH-1:0] 
//    xyz = replacement_block_i;  //    [BLOCK_WIDTH-1:0] 
//    xyz = write_back_i;  //     
//    xyz = valid_i;  //     
//    xyz = dirty_i;  //     
//    xyz = way_sel_i;  //    [WAY_SEL_WIDTH-1:0] 
//    xyz = tag_out_i;  //    [ADDR_TAG_FIELD_WIDTH-1:0] 

      // TODO: CHANGED
      //-start_time = $time;
      address = address_i;    //    [ADDRESS_WIDTH-1:0] 
      mem_write = mem_write_i;//     
      mem_read = mem_read_i;  //
      input_block = input_block_i;
      valid = valid_i;
      dirty = dirty_i;
      way_sel = way_sel_i;
      tag_out = tag_out_i;
      replacement_block = replacement_block_i;
      write_back = write_back_i;
      //-end_time = $time;             

     endtask         
  
endinterface
