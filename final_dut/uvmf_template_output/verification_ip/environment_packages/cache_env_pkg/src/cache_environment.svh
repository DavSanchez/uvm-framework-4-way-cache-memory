//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Environment 
// Unit            : cache Environment
// File            : cache_environment.svh
//----------------------------------------------------------------------
//                                          
// DESCRIPTION: This environment contains all agents, predictors and
// scoreboards required for the block level design.
//----------------------------------------------------------------------
//



class cache_environment 
extends uvmf_environment_base #(.CONFIG_T( cache_env_configuration
                             ));

  `uvm_component_utils( cache_environment );





  typedef cpu_agent cpu_agent_agent_t;
  cpu_agent_agent_t cpu_agent;

  typedef mem_agent mem_agent_agent_t;
  mem_agent_agent_t mem_agent;


  typedef cache_predictor  cache_pred_t;
  cache_pred_t cache_pred;

  typedef uvmf_in_order_scoreboard #(cpu_transaction)  cpu_agent_sb_t;
  cpu_agent_sb_t cpu_agent_sb;
  typedef uvmf_in_order_scoreboard #(mem_transaction)  mem_agent_sb_t;
  mem_agent_sb_t mem_agent_sb;



// ****************************************************************************
// FUNCTION : new()
// This function is the standard SystemVerilog constructor.
//
  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

// ****************************************************************************
// FUNCTION: build_phase()
// This function builds all components within this environment.
//
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    cpu_agent = cpu_agent_agent_t::type_id::create("cpu_agent",this);
    mem_agent = mem_agent_agent_t::type_id::create("mem_agent",this);
    cache_pred = cache_pred_t::type_id::create("cache_pred",this);
    cache_pred.configuration = configuration;
    cpu_agent_sb = cpu_agent_sb_t::type_id::create("cpu_agent_sb",this);
    mem_agent_sb = mem_agent_sb_t::type_id::create("mem_agent_sb",this);


  endfunction

// ****************************************************************************
// FUNCTION: connect_phase()
// This function makes all connections within this environment.  Connections
// typically inclue agent to predictor, predictor to scoreboard and scoreboard
// to agent.
//
  virtual function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    cpu_agent.monitored_ap.connect(cache_pred.cpu_agent_ae);
    mem_agent.monitored_ap.connect(cache_pred.mem_agent_ae);
    cache_pred.cpu_agent_sb_ap.connect(cpu_agent_sb.expected_analysis_export);
    cache_pred.mem_agent_sb_ap.connect(mem_agent_sb.expected_analysis_export);
    cpu_agent.monitored_ap.connect(cpu_agent_sb.actual_analysis_export);
    mem_agent.monitored_ap.connect(mem_agent_sb.actual_analysis_export);


  endfunction

endclass

