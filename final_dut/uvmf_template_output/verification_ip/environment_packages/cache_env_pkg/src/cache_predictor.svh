//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache_predictor 
// Unit            : cache_predictor 
// File            : cache_predictor.svh
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//
//
// DESCRIPTION: This analysis component contains analysis_exports for receiving
//   data and analysis_ports for sending data.
// 
//   This analysis component has the following analysis_exports that receive the 
//   listed transaction type.
//   
//   cpu_agent_ae receives transactions of type  cpu_transaction #()  
//   mem_agent_ae receives transactions of type  mem_transaction #()  
//
//   This analysis component has the following analysis_ports that can broadcast 
//   the listed transaction type.
//
//  mem_agent_sb_ap broadcasts transactions of type mem_transaction #() 
//  cpu_agent_sb_ap broadcasts transactions of type cpu_transaction #() 
//

class cache_predictor 
extends uvm_component;

  // Factory registration of this class
  `uvm_component_utils( cache_predictor );


  // Instantiate a handle to the configuration of the environment in which this component resides
  cache_env_configuration  configuration;

  // Instantiate the analysis exports
  uvm_analysis_imp_cpu_agent_ae #(cpu_transaction #(), cache_predictor ) cpu_agent_ae;
  uvm_analysis_imp_mem_agent_ae #(mem_transaction #(), cache_predictor ) mem_agent_ae;

  // Instantiate the analysis ports
  uvm_analysis_port #(mem_transaction #()) mem_agent_sb_ap;
  uvm_analysis_port #(cpu_transaction #()) cpu_agent_sb_ap;

  // Transaction variable for predicted values to be sent out mem_agent_sb_ap
  mem_transaction #() mem_agent_sb_ap_output_transaction;
  // Code for sending output transaction out through mem_agent_sb_ap
  // mem_agent_sb_ap.write(mem_agent_sb_ap_output_transaction);

  // Transaction variable for predicted values to be sent out cpu_agent_sb_ap
  cpu_transaction #() cpu_agent_sb_ap_output_transaction;
  // Code for sending output transaction out through cpu_agent_sb_ap
  // cpu_agent_sb_ap.write(cpu_agent_sb_ap_output_transaction);


  // FUNCTION: new
  function new(string name, uvm_component parent);
     super.new(name,parent);
  endfunction

  // FUNCTION: build_phase
  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);

    cpu_agent_ae = new("cpu_agent_ae", this);
    mem_agent_ae = new("mem_agent_ae", this);

    mem_agent_sb_ap =new("mem_agent_sb_ap", this );
    cpu_agent_sb_ap =new("cpu_agent_sb_ap", this );

  endfunction

  // FUNCTION: write_cpu_agent_ae
  // Transactions received through cpu_agent_ae initiate the execution of this function.
  // This function performs prediction of DUT output values based on DUT input, configuration and state
  virtual function void write_cpu_agent_ae(cpu_transaction #() t);
    `uvm_info("COV", "Transaction Received through cpu_agent_ae", UVM_MEDIUM)
    `uvm_info("COV", {"            Data: ",t.convert2string()}, UVM_FULL)

    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "******************************************************************************************************",UVM_NONE)
    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "The cache_predictor::write_cpu_agent_ae function needs to be completed with DUT prediction model",UVM_NONE)
    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "******************************************************************************************************",UVM_NONE)

 

  // Construct one of each output transaction type.
  mem_agent_sb_ap_output_transaction = mem_transaction #()::type_id::create("mem_agent_sb_ap_output_transaction");
  cpu_agent_sb_ap_output_transaction = cpu_transaction #()::type_id::create("cpu_agent_sb_ap_output_transaction");

    // Code for sending output transaction out through mem_agent_sb_ap
    mem_agent_sb_ap.write(mem_agent_sb_ap_output_transaction);
    // Code for sending output transaction out through cpu_agent_sb_ap
    cpu_agent_sb_ap.write(cpu_agent_sb_ap_output_transaction);
  endfunction

  // FUNCTION: write_mem_agent_ae
  // Transactions received through mem_agent_ae initiate the execution of this function.
  // This function performs prediction of DUT output values based on DUT input, configuration and state
  virtual function void write_mem_agent_ae(mem_transaction #() t);
    `uvm_info("COV", "Transaction Received through mem_agent_ae", UVM_MEDIUM)
    `uvm_info("COV", {"            Data: ",t.convert2string()}, UVM_FULL)

    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "******************************************************************************************************",UVM_NONE)
    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "The cache_predictor::write_mem_agent_ae function needs to be completed with DUT prediction model",UVM_NONE)
    `uvm_info("RED_ALERT:UNIMPLEMENTED_PREDICTOR_MODEL", "******************************************************************************************************",UVM_NONE)

 

  // Construct one of each output transaction type.
  mem_agent_sb_ap_output_transaction = mem_transaction #()::type_id::create("mem_agent_sb_ap_output_transaction");
  cpu_agent_sb_ap_output_transaction = cpu_transaction #()::type_id::create("cpu_agent_sb_ap_output_transaction");

    // Code for sending output transaction out through mem_agent_sb_ap
    mem_agent_sb_ap.write(mem_agent_sb_ap_output_transaction);
    // Code for sending output transaction out through cpu_agent_sb_ap
    cpu_agent_sb_ap.write(cpu_agent_sb_ap_output_transaction);
  endfunction

endclass 

