//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache environment agent
// Unit            : Environment HVL package
// File            : cache_pkg.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    environment package that will run on the host simulator.
//
// CONTAINS:
//     - <cache_configuration.svh>
//     - <cache_environment.svh>
//     - <cache_env_sequence_base.svh>
//     - <cache_infact_env_sequence.svh>
//     - <cache_predictor.svh>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package cache_env_pkg;

   import uvm_pkg::*;
   import questa_uvm_pkg::*;
   `include "uvm_macros.svh"

   import uvmf_base_pkg::*;
   import cpu_pkg::*;
   import mem_pkg::*;




 

   `uvm_analysis_imp_decl(_cpu_agent_ae)
   `uvm_analysis_imp_decl(_mem_agent_ae)

   `include "src/cache_env_typedefs.svh"

   `include "src/cache_env_configuration.svh"
   `include "src/cache_predictor.svh"
   `include "src/cache_environment.svh"
   `include "src/cache_env_sequence_base.svh"
   `include "src/cache_infact_env_sequence.svh"
  
endpackage

