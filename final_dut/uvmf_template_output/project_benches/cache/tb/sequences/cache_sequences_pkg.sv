//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : Sequences Package
// File            : cache_sequences_pkg.sv
//----------------------------------------------------------------------
//
// DESCRIPTION: This package includes all high level sequence classes used 
//     in the environment.  These include utility sequences and top
//     level sequences.
//
// CONTAINS:
//     -<cache_sequence_base>
//     -<example_derived_test_sequence>
//
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
//

package cache_sequences_pkg;

   import uvm_pkg::*;
   import questa_uvm_pkg::*;
   import uvmf_base_pkg::*;
   import cpu_pkg::*;
   import mem_pkg::*;
   import cache_parameters_pkg::*;
   import cache_env_pkg::*;

  
   `include "uvm_macros.svh"

   `include "src/cache_bench_sequence_base.svh"
   `include "src/infact_bench_sequence.svh"
   `include "src/example_derived_test_sequence.svh"

   `include "src/cache_read_sequence.svh"    // CHANGED. ADDED READ VIRTUAL SEQUENCE
   `include "src/cache_write_sequence.svh"   // CHANGED. ADDED WRITE VIRTUAL SEQUENCE

   `include "src/cache_write_back_sequence.svh" // CHANGED. ADDED WRITE BACK SEQUENCE

endpackage

