//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 13
// 
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench
// Unit            : Bench Virtual Sequence
// File            : cache_write_back_sequence.svh
//----------------------------------------------------------------------
//     
// Description: This file contains the top level and utility sequences
//     used by write_back_test.
//
// ****************************************************************************
// 

class cache_write_back_sequence extends cache_bench_sequence_base;

    `uvm_object_utils( cache_write_back_sequence );

    typedef cpu_read_burst_sequence cpu_agent_read_burst_seq_t;
    cpu_agent_read_burst_seq_t cpu_agent_read_burst_s;
    typedef cpu_write_burst_sequence cpu_agent_write_burst_seq_t;
    cpu_agent_write_burst_seq_t cpu_agent_write_burst_s;

    rand bit [3:0] index_f;
    rand bit [23:0] tag_f_array[10];

    function new( string name = "" );
        super.new( name );
    endfunction

    virtual task body();

        if (!std::randomize(index_f, tag_f_array)) `uvm_fatal("WBSEQ", "cache_write_back_sequence::body()-index or tag array randomization failed")

        // Construct sequences here
        cpu_agent_random_seq     = cpu_agent_random_seq_t::type_id::create("cpu_agent_random_seq");
        mem_agent_random_seq     = mem_agent_random_seq_t::type_id::create("mem_agent_random_seq");

        // Read sequence
        cpu_agent_read_burst_s    = cpu_agent_read_burst_seq_t::type_id::create("cpu_agent_read_burst_s");
        cpu_agent_write_burst_s   = cpu_agent_write_burst_seq_t::type_id::create("cpu_agent_write_burst_s");

        // TODO: CHANGED. Constructed responder sequence for memory
        mem_agent_responder_seq  = mem_agent_responder_seq_t::type_id::create("mem_agent_responder_seq");

        fork
            cpu_agent_config.wait_for_reset();
            cpu_agent_config.wait_for_num_clocks(10);
            mem_agent_config.wait_for_reset();
            mem_agent_config.wait_for_num_clocks(10);
        join
        
        // Start RESPONDER sequences here
        fork
            mem_agent_responder_seq.start(mem_agent_sequencer);
        join_none
        
        // Start INITIATOR sequences here
        fork
            begin
                cpu_agent_read_burst_s.index_field = index_f;
                cpu_agent_write_burst_s.index_field = index_f;

                foreach (tag_f_array[i]) begin
                    cpu_agent_read_burst_s.tag_field = tag_f_array[i];
                    cpu_agent_write_burst_s.tag_field = tag_f_array[i];

                    // TEN CONSECUTIVE READS TO COMPLETELY POPULATE A SET LINES
                    $display("Read sequence burst. Sequence: %d. Tag array value: 0x%h. Index value: 0x%h.", i, tag_f_array[i], index_f);
                    cpu_agent_read_burst_s.start(cpu_agent_sequencer);
                    // TEN CONSECUTIVE WRITES TO UPDATE THESE READ LINES
                    $display("Write sequence burst. Sequence: %d. Tag array value: 0x%h. Index value: 0x%h.", i, tag_f_array[i], index_f);
                    cpu_agent_write_burst_s.start(cpu_agent_sequencer);
                end
        
                // AN ADDITIONAL READ TO SEE IF A LINE GETS UPDATED WITH WRITE-BACK POLICY
                cpu_agent_read_burst_s.tag_field = tag_f_array[0];
                $display("Final read sequence. Tag array value: 0x%h. Index value: 0x%h.", tag_f_array[0], index_f);
                cpu_agent_read_burst_s.start(cpu_agent_sequencer);
            end
        join
        
        fork
            cpu_agent_config.wait_for_num_clocks(50);
            mem_agent_config.wait_for_num_clocks(50);
        join

    endtask

endclass
