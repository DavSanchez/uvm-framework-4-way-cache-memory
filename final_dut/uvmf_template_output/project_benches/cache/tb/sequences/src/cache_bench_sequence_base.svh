//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : Bench Sequence Base
// File            : cache_bench_sequence_base.svh
//----------------------------------------------------------------------
//
// Description: This file contains the top level and utility sequences
//     used by test_top. It can be extended to create derivative top
//     level sequences.
//
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
//

class cache_bench_sequence_base extends uvmf_sequence_base #(uvm_sequence_item);

  `uvm_object_utils( cache_bench_sequence_base );

  // UVMF_CHANGE_ME : Instantiate, construct, and start sequences as needed to create stimulus scenarios.

  // Instantiate sequences here
typedef cpu_random_sequence  cpu_agent_random_seq_t;
cpu_agent_random_seq_t cpu_agent_random_seq;
typedef mem_random_sequence  mem_agent_random_seq_t;
mem_agent_random_seq_t mem_agent_random_seq;

  // TODO: CHANGED. Instantiate RESPONDER sequence for memory agent
typedef mem_responder_sequence mem_agent_responder_seq_t;
mem_agent_responder_seq_t mem_agent_responder_seq;

  // Sequencer handles for each active interface in the environment
typedef cpu_transaction  cpu_agent_transaction_t;
uvm_sequencer #(cpu_agent_transaction_t)  cpu_agent_sequencer; 
typedef mem_transaction  mem_agent_transaction_t;
uvm_sequencer #(mem_agent_transaction_t)  mem_agent_sequencer; 

// Sequencer handles for each QVIP interface

// Configuration handles to access interface BFM's
cpu_configuration   cpu_agent_config;
mem_configuration   mem_agent_config;




// ****************************************************************************
  function new( string name = "" );
     super.new( name );

  // Retrieve the configuration handles from the uvm_config_db
if( !uvm_config_db #( cpu_configuration )::get( null , UVMF_CONFIGURATIONS , cpu_pkg_cpu_agent_BFM , cpu_agent_config ) ) 
`uvm_error("CFG" , "uvm_config_db #( cpu_configuration )::get cannot find resource cpu_pkg_cpu_agent_BFM" )
if( !uvm_config_db #( mem_configuration )::get( null , UVMF_CONFIGURATIONS , mem_pkg_mem_agent_BFM , mem_agent_config ) ) 
`uvm_error("CFG" , "uvm_config_db #( mem_configuration )::get cannot find resource mem_pkg_mem_agent_BFM" )

  // Retrieve the sequencer handles from the uvm_config_db
if( !uvm_config_db #( uvm_sequencer #(cpu_agent_transaction_t) )::get( null , UVMF_SEQUENCERS , cpu_pkg_cpu_agent_BFM , cpu_agent_sequencer ) ) 
`uvm_error("CFG" , "uvm_config_db #( uvm_sequencer #(cpu_transaction) )::get cannot find resource cpu_pkg_cpu_agent_BFM" ) 
if( !uvm_config_db #( uvm_sequencer #(mem_agent_transaction_t) )::get( null , UVMF_SEQUENCERS , mem_pkg_mem_agent_BFM , mem_agent_sequencer ) ) 
`uvm_error("CFG" , "uvm_config_db #( uvm_sequencer #(mem_transaction) )::get cannot find resource mem_pkg_mem_agent_BFM" ) 

  // Retrieve QVIP sequencer handles from the uvm_config_db



  endfunction


// ****************************************************************************
  virtual task body();

  // Construct sequences here
   cpu_agent_random_seq     = cpu_agent_random_seq_t::type_id::create("cpu_agent_random_seq");
   mem_agent_random_seq     = mem_agent_random_seq_t::type_id::create("mem_agent_random_seq");

  // TODO: CHANGED. Constructed responder sequence for memory
   mem_agent_responder_seq  = mem_agent_responder_seq_t::type_id::create("mem_agent_responder_seq");


   fork
    cpu_agent_config.wait_for_reset();
    mem_agent_config.wait_for_reset();
   join

  // Start RESPONDER sequences here
   fork
    mem_agent_responder_seq.start(mem_agent_sequencer);
   join_none

  // Start INITIATOR sequences here
   fork
       repeat (25) cpu_agent_random_seq.start(cpu_agent_sequencer);
       //repeat (25) mem_agent_random_seq.start(mem_agent_sequencer);
   join


   // UVMF_CHANGE_ME : Extend the simulation XXX number of clocks after 
   // the last sequence to allow for the last sequence item to flow 
   // through the design.

  fork
    cpu_agent_config.wait_for_num_clocks(400);
    mem_agent_config.wait_for_num_clocks(400);
  join

  endtask

endclass

