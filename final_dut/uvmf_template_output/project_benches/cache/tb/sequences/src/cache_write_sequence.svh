//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 13
// 
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench
// Unit            : Bench Virtual Sequence
// File            : cache_write_sequence.svh
//----------------------------------------------------------------------
//     
// Description: This file contains the top level and utility sequences
//     used by write_test.
//
// ****************************************************************************
// 

class cache_write_sequence extends cache_bench_sequence_base;

    `uvm_object_utils( cache_write_sequence );

    typedef cpu_write_sequence cpu_agent_write_seq_t;
    cpu_agent_write_seq_t cpu_agent_write_s;

    function new( string name = "" );
        super.new( name );
    endfunction

    virtual task body();
        // Construct sequences here
        cpu_agent_random_seq     = cpu_agent_random_seq_t::type_id::create("cpu_agent_random_seq");
        mem_agent_random_seq     = mem_agent_random_seq_t::type_id::create("mem_agent_random_seq");

        // Write sequence
        cpu_agent_write_s = cpu_agent_write_seq_t::type_id::create("cpu_agent_write_s");

        // TODO: CHANGED. Constructed responder sequence for memory
        mem_agent_responder_seq  = mem_agent_responder_seq_t::type_id::create("mem_agent_responder_seq");

        fork
            cpu_agent_config.wait_for_reset();
            cpu_agent_config.wait_for_num_clocks(10);
            mem_agent_config.wait_for_reset();
            mem_agent_config.wait_for_num_clocks(10);
        join
        
        // Start RESPONDER sequences here
        fork
            mem_agent_responder_seq.start(mem_agent_sequencer);
        join_none
        
        // Start INITIATOR sequences here
        fork
            cpu_agent_write_s.start(cpu_agent_sequencer);
        join
        
        fork
            cpu_agent_config.wait_for_num_clocks(50);
            mem_agent_config.wait_for_num_clocks(50);
        join

    endtask

endclass
