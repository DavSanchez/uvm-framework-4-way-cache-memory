//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : HDL top level module
// File            : hdl_top.sv
//----------------------------------------------------------------------
//                                          
// Description: This top level module instantiates all synthesizable
//    static content.  This and tb_top.sv are the two top level modules
//    of the simulation.  
//
//    This module instantiates the following:
//        DUT: The Design Under Test
//        Interfaces:  Signal bundles that contain signals connected to DUT
//        Driver BFM's: BFM's that actively drive interface signals
//        Monitor BFM's: BFM's that passively monitor interface signals
//
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
//

import cache_parameters_pkg::*;
import uvmf_base_pkg_hdl::*;



module hdl_top;
// pragma attribute hdl_top partition_module_xrtl                                            


bit rst = 1;
bit clk;
   // Instantiate a clk driver 
   // tbx clkgen
   initial begin
      #9ns;
      clk = ~clk;
      forever #5ns clk = ~clk;
   end
   // Instantiate a rst driver
   initial begin
      #200ns;
      rst <= ~rst;
   end



// Instantiate the signal bundle, monitor bfm and driver bfm for each interface.
// The signal bundle, _if, contains signals to be connected to the DUT.
// The monitor, monitor_bfm, observes the bus, _if, and captures transactions.
// The driver, driver_bfm, drives transactions onto the bus, _if.

cpu_if  cpu_agent_bus(.clk(clk), .rst(rst));
mem_if  mem_agent_bus(.clk(clk),
                      .rst(rst),
                      .address(cpu_agent_bus.address),
                      .mem_read(cpu_agent_bus.mem_read),
                      .mem_write(cpu_agent_bus.mem_write));

cpu_monitor_bfm  cpu_agent_mon_bfm(cpu_agent_bus);
mem_monitor_bfm  mem_agent_mon_bfm(mem_agent_bus);

cpu_driver_bfm  cpu_agent_drv_bfm(cpu_agent_bus);
mem_driver_bfm  mem_agent_drv_bfm(mem_agent_bus);


// UVMF_CHANGE_ME : Add DUT and connect to signals in _bus interfaces listed above
// Instantiate DUT here
Cache #(
   // .WORD_WIDTH(8),
   // .ADDRESS_WIDTH(32),
   // .BLOCK_WIDTH(128),
   // .ADDR_WORD_FIELD_WIDTH(4),
   // .ADDR_INDEX_FIELD_WIDTH(4),
   // .ADDR_TAG_FIELD_WIDTH(24),
   // .WAY_SEL_WIDTH(2)
   ) DUT (
   .clk                    (~cpu_agent_bus.clk),
   .reset                  (cpu_agent_bus.rst),
   .byteToWrite            (cpu_agent_bus.byte_to_write),
   .inputBlock             (mem_agent_bus.input_block),
   .address                (cpu_agent_bus.address),
   .memWrite               (cpu_agent_bus.mem_write),
   .memRead                (cpu_agent_bus.mem_read),
   .hit                    (cpu_agent_bus.hit),
   .valid                  (mem_agent_bus.valid),
   .dirty                  (mem_agent_bus.dirty),
   .waySel                 (mem_agent_bus.way_sel),
   .tag_out                (mem_agent_bus.tag_out),
   .replacementBlock       (mem_agent_bus.replacement_block),
   .byteDataOutFromCache   (cpu_agent_bus.byte_data_out_from_cache),
   .writeBackSignal        (mem_agent_bus.write_back)
);

initial begin  // tbx vif_binding_block 
    import uvm_pkg::uvm_config_db;
// The monitor_bfm and driver_bfm for each interface is placed into the uvm_config_db.
// They are placed into the uvm_config_db using the string names defined in the parameters package.
// The string names are passed to the agent configurations by test_top through the top level configuration.
// They are retrieved by the agents configuration class for use by the agent.

uvm_config_db #( virtual cpu_monitor_bfm  )::set( null , UVMF_VIRTUAL_INTERFACES , cpu_pkg_cpu_agent_BFM , cpu_agent_mon_bfm ); 
uvm_config_db #( virtual mem_monitor_bfm  )::set( null , UVMF_VIRTUAL_INTERFACES , mem_pkg_mem_agent_BFM , mem_agent_mon_bfm ); 

uvm_config_db #( virtual cpu_driver_bfm  )::set( null , UVMF_VIRTUAL_INTERFACES , cpu_pkg_cpu_agent_BFM , cpu_agent_drv_bfm  );
uvm_config_db #( virtual mem_driver_bfm  )::set( null , UVMF_VIRTUAL_INTERFACES , mem_pkg_mem_agent_BFM , mem_agent_drv_bfm  );


  end

endmodule

