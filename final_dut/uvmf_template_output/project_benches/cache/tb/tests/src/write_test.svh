//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 13
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : Top level UVM Test
// File            : write_test.svh
//----------------------------------------------------------------------
//                                          
// DESCRIPTION: This test extends test_top and makes the following
//    changes to test_top using the UVM factory type_override:
//
//
//----------------------------------------------------------------------
//

class write_test extends test_top;

  `uvm_component_utils( write_test );

  function new( string name = "", uvm_component parent = null );
    super.new( name, parent );
  endfunction

  virtual function void build_phase(uvm_phase phase);
    // The factory override below is an example of how to replace the cache_bench_sequence_base 
    // sequence with the example_derived_test_sequence.
    cache_bench_sequence_base::type_id::set_type_override(cache_write_sequence::get_type());
    // Execute the build_phase of test_top AFTER all factory overrides have been created.
    super.build_phase(phase);
  endfunction

endclass

