//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : Top level UVM test
// File            : test_top.svh
//----------------------------------------------------------------------
// Description: This top level UVM test is the base class for all
//     future tests created for this project.
//
//     This test class contains:
//          Configuration:  The top level configuration for the project.
//          Environment:    The top level environment for the project.
//          Top_level_sequence:  The top level sequence for the project.
//                                          
//----------------------------------------------------------------------
//

typedef cache_env_configuration cache_env_configuration_t;
typedef cache_environment cache_environment_t;

class test_top extends uvmf_test_base #(.CONFIG_T(cache_env_configuration_t), 
                                        .ENV_T(cache_environment_t), 
                                        .TOP_LEVEL_SEQ_T(cache_bench_sequence_base));

  `uvm_component_utils( test_top );


string interface_names[] = {
    cpu_pkg_cpu_agent_BFM /* cpu_agent     [0] */ , 
    mem_pkg_mem_agent_BFM /* mem_agent     [1] */ 
};

uvmf_active_passive_t interface_activities[] = { 
    ACTIVE /* cpu_agent     [0] */ , 
    ACTIVE /* mem_agent     [1] */ 
};

// ****************************************************************************
// FUNCTION: new()
// This is the standard system verilog constructor.  All components are 
// constructed in the build_phase to allow factory overriding.
//
  function new( string name = "", uvm_component parent = null );
     super.new( name ,parent );
  endfunction




// ****************************************************************************
// FUNCTION: build_phase()
// The construction of the configuration and environment classes is done in
// the build_phase of uvmf_test_base.  Once the configuraton and environment
// classes are built then the initialize call is made to perform the
// following: 
//     Monitor and driver BFM virtual interface handle passing into agents
//     Set the active/passive state for each agent
// Once this build_phase completes, the build_phase of the environment is
// executed which builds the agents.
//
  virtual function void build_phase(uvm_phase phase);

    super.build_phase(phase);
    configuration.initialize(BLOCK, "uvm_test_top.environment", interface_names, null, interface_activities);



  endfunction

endclass

