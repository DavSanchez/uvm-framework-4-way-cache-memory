//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2019 Mar 07
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : cache Simulation Bench 
// Unit            : Bench level parameters package
// File            : cache_parameters_pkg.sv
//----------------------------------------------------------------------
// 
//                                         
//----------------------------------------------------------------------
//

package cache_parameters_pkg;

import uvmf_base_pkg_hdl::*;


// These parameters are used to uniquely identify each interface.  The monitor_bfm and
// driver_bfm are placed into and retrieved from the uvm_config_db using these string 
// names as the field_name. The parameter is also used to enable transaction viewing 
// from the command line for selected interfaces using the UVM command line processing.

parameter string cpu_pkg_cpu_agent_BFM  = "cpu_pkg_cpu_agent_BFM"; /* [0] */
parameter string mem_pkg_mem_agent_BFM  = "mem_pkg_mem_agent_BFM"; /* [1] */



endpackage

