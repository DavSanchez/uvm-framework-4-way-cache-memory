#! /usr/bin/env python
# coding=utf-8

# Importando el generador de código UVMF
import uvmf_gen

# Nombre del paquete de protocolo
intf = uvmf_gen.InterfaceClass('mem')

# Desactivando suporte para inFact en el Makefile
intf.inFactReady = False

# Parámetros para esta interfaz
intf.addParamDef('ADDRESS_WIDTH', 'int', '32')
intf.addParamDef('BLOCK_WIDTH', 'int', '128')
intf.addParamDef('WAY_SEL_WIDTH', 'int', '2')
intf.addParamDef('WORD_WIDTH', 'int', '8')
intf.addParamDef('ADDR_WORD_FIELD_WIDTH', 'int', '4')
intf.addParamDef('ADDR_INDEX_FIELD_WIDTH', 'int', '4')
intf.addParamDef('ADDR_TAG_FIELD_WIDTH', 'int', '24')

# Reloj y reset
intf.clock = 'clk'
intf.reset = 'rst'
intf.resetAssertionLevel = True  # ACTIVO A NIVEL ALTO

# Añadiendo señales a la interfaz NOTE: Entrada o salida desde la perspectiva del BENCH!!
# Señales procedentes de la CPU (Por ser agente de tipo RESPONDER)
intf.addPort('address', 'ADDRESS_WIDTH', 'output')
intf.addPort('mem_write', 1, 'output')
intf.addPort('mem_read', 1, 'output')
# Entradas a la caché
intf.addPort('input_block', 'BLOCK_WIDTH', 'input')
# Salidas de la caché a memoria (y a controlador)
intf.addPort('replacement_block', 'BLOCK_WIDTH', 'output')
intf.addPort('write_back', 1, 'output')
intf.addPort('valid', 1, 'output')
intf.addPort('dirty', 1, 'output')
intf.addPort('way_sel', 'WAY_SEL_WIDTH', 'output')
intf.addPort('tag_out', 'ADDR_TAG_FIELD_WIDTH', 'output')

# Especificando variables de transacción
# NOTE un bloque entrante de memoria para una direccion determinada NO ES RANDOM. Interfaz reactiva.

# De la CPU. iscompare=False porque ya el agente de CPU los compara
intf.addTransVar('address', 'bit [ADDRESS_WIDTH-1:0]', isrand=False, iscompare=True)
intf.addTransVar('mem_write', 'bit', isrand=False, iscompare=True)
intf.addTransVar('mem_read', 'bit', isrand=False, iscompare=True)
# Entrada
intf.addTransVar('input_block', 'bit [BLOCK_WIDTH-1:0]', isrand=True, iscompare=True)
# Salidas
intf.addTransVar('replacement_block', 'bit [BLOCK_WIDTH-1:0]', isrand=False, iscompare=True)
intf.addTransVar('write_back', 'bit', isrand=False, iscompare=True)
intf.addTransVar('valid', 'bit', isrand=False, iscompare=True)
intf.addTransVar('dirty', 'bit', isrand=False, iscompare=True)
intf.addTransVar('way_sel', 'bit [WAY_SEL_WIDTH-1:0]', isrand=False, iscompare=True)
intf.addTransVar('tag_out', 'bit [ADDR_TAG_FIELD_WIDTH-1:0]', isrand=False, iscompare=True)

# Funciones de RESPONDER. Condición de respuesta y dato enviado
intf.specifyResponseOperation('txn.mem_read != txn.mem_write')
intf.specifyResponseData(['input_block'])

# Añadiendo restricciones
# intf.addTransVarConstraint('nombre','definicion')

# Añadiendo variables a la clase de configuracion
# intf.addConfigVar('nombre','tipo',(opcional)isrand)

# Añadiendo restricciones a las variables de configuracion
# intf.addConfigVarConstraint('nombre','definicion')

intf.create()
