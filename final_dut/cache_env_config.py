#! /usr/bin/env python
# coding=utf-8

# Importando el generador de código UVMF
import uvmf_gen

# Definiendo el paquete del entorno
env = uvmf_gen.EnvironmentClass('cache')

# Especificando parámetros
# env.addParamDef(nombre, tipo, valor)

# Especificando los agentes contenidos en este entorno
# env.addAgent(nombreManejador, nombrePaquete, nombreReloj, nombreReset)
env.addAgent('cpu_agent', 'cpu', 'clk', 'rst')
env.addAgent('mem_agent', 'mem', 'clk', 'rst', {}, initResp = 'RESPONDER') # Comunicación CPU

# Definiendo el predictor contenido en el entorno (aun no instanciado)
# env.defineAnalysisComponent(keyword,nombreTipoPredictor, dictExports, dictPort)
env.defineAnalysisComponent('predictor', 'cache_predictor',
                           {'cpu_agent_ae': 'cpu_transaction #()',
                            'mem_agent_ae': 'mem_transaction #()'},
                           {'cpu_agent_sb_ap': 'cpu_transaction #()',
                            'mem_agent_sb_ap': 'mem_transaction #()'})

# Instanciando el predictor
# env.addAnalysisComponent(nombre,tipo)
env.addAnalysisComponent('cache_pred', 'cache_predictor')

# Especificando scoreboards UVMF al entorno
# env.addUvmfScoreboard(nombreManejador, nombreTipo, nombreTipoTransaccion)
env.addUvmfScoreboard('cpu_agent_sb', 'uvmf_in_order_scoreboard', 'cpu_transaction')
env.addUvmfScoreboard('mem_agent_sb', 'uvmf_in_order_scoreboard', 'mem_transaction')

# Añadiendo conexiones
# env.addConnection(componenteOutput,nombrePuertoComponenteOutput,componenteInput,nombreExportComponenteInput)
env.addConnection('cpu_agent', 'monitored_ap', 'cache_pred', 'cpu_agent_ae')   # Connection 00
env.addConnection('mem_agent', 'monitored_ap', 'cache_pred', 'mem_agent_ae')   # Connection 01
env.addConnection('cache_pred', 'cpu_agent_sb_ap', 'cpu_agent_sb', 'expected_analysis_export') # Connection 02
env.addConnection('cache_pred', 'mem_agent_sb_ap', 'mem_agent_sb', 'expected_analysis_export') # Connection 03
env.addConnection('cpu_agent', 'monitored_ap', 'cpu_agent_sb', 'actual_analysis_export') # Connection 04
env.addConnection('mem_agent', 'monitored_ap', 'mem_agent_sb', 'actual_analysis_export') # Connection 05

# Añadiendo variables a la clase de configuracion para el entorno
# env.addConfigVar(nombre,tipo)

# Añadiendo restricciones a la clase de configuracion del entorno
# env.addConfigVarConstraint(nombre,definicion)

# Creando el entorno
env.create()
