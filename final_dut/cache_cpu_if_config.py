#! /usr/bin/env python
# coding=utf-8

# Importando el generador de código UVMF
import uvmf_gen

# Nombre del paquete de protocolo
intf = uvmf_gen.InterfaceClass('cpu')

# Desactivando suporte para inFact en el Makefile
intf.inFactReady = False

# Parámetros para esta interfaz
intf.addParamDef('WORD_WIDTH', 'int', '8')
intf.addParamDef('ADDRESS_WIDTH', 'int', '32')
intf.addParamDef('ADDR_WORD_FIELD_WIDTH', 'int', '4')
intf.addParamDef('ADDR_INDEX_FIELD_WIDTH', 'int', '4')
intf.addParamDef('ADDR_TAG_FIELD_WIDTH', 'int', '24')

# Reloj y reset
intf.clock = 'clk'
intf.reset = 'rst'
intf.resetAssertionLevel = True  # ACTIVO A NIVEL ALTO

# Añadiendo señales a la interfaz
# Entradas a la caché
#intf.addPort('cache_rst', 1, 'output')
intf.addPort('byte_to_write', 'WORD_WIDTH', 'output')
intf.addPort('address', 'ADDRESS_WIDTH', 'output')
intf.addPort('mem_write', 1, 'output')
intf.addPort('mem_read', 1, 'output')
# Salidas de la caché
intf.addPort('hit', 1, 'input')
intf.addPort('byte_data_out_from_cache', 'WORD_WIDTH', 'input')

# Especificando variables de transacción
# Entradas
intf.addTransVar('byte_to_write', 'byte unsigned', isrand=True, iscompare=True)
# Construyendo la direccion a partir de sus campos: [[TAG][INDEX][WORD]]
intf.addTransVar('address_word', 'bit [ADDR_WORD_FIELD_WIDTH-1:0]', isrand=True, iscompare=False)
intf.addTransVar('address_index', 'bit [ADDR_INDEX_FIELD_WIDTH-1:0]', isrand=True, iscompare=False)
intf.addTransVar('address_tag', 'bit [ADDR_TAG_FIELD_WIDTH-1:0]', isrand=True, iscompare=False)
# Address no es random porque se construye concatenando los tres anteriores
intf.addTransVar('address', 'bit [ADDRESS_WIDTH-1:0]', isrand=False, iscompare=True)
intf.addTransVar('mem_write', 'bit', isrand=True, iscompare=True)
intf.addTransVar('mem_read', 'bit', isrand=True, iscompare=True)
# Salidas
intf.addTransVar('hit', 'bit', isrand=False, iscompare=True)
intf.addTransVar('byte_data_out_from_cache', 'byte unsigned', isrand=False, iscompare=True)

## Specify transaction variable constraint
## addTransVarConstraint(<constraint_body_name>,<constraint_body_definition>)
# intf.addTransVarConstraint('no_read_and_write','{ mem_read != mem_write; }') // TODO: Good practice or better inline?

# Añadiendo restricciones
# intf.addTransVarConstraint('nombre','definicion')

# Añadiendo variables a la clase de configuracion
# intf.addConfigVar('nombre','tipo',(opcional)isrand)

# Añadiendo restricciones a las variables de configuracion
# intf.addConfigVarConstraint('nombre','definicion')

intf.create()
