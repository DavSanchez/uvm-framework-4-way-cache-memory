#! /usr/bin/env python
# coding=utf-8

import uvmf_gen

# El parámetro de esta función es el nombre de la interfaz que vamos a crear
intf = uvmf_gen.InterfaceClass('alu_out')
intf.inFactReady = False

# Parámetros para la interfaz
# Usados para definir tamaños de señales y variables
intf.addParamDef('ALU_OUT_RESULT_WIDTH', 'int', '16')

# señales de reloj y reset para la interfaz
intf.clock = 'clk'
intf.reset = 'rst'
intf.resetAssertionLevel = False  # Reset es activo a nivel bajo

# Puertos asociados a la interfaz
# Declarados con respecto al Testbench
intf.addPort('done', 1, 'input')
intf.addPort('result', 'ALU_OUT_RESULT_WIDTH', 'input')

# Variables de transacción para la interfaz
intf.addTransVar(
    'result', 'bit [ALU_OUT_RESULT_WIDTH-1:0]', isrand=False, iscompare=True)

# SE CREAN EXPLÍCITAMENTE TODOS LOS FICHEROS SV DE LAS INTERFACES
intf.create()
