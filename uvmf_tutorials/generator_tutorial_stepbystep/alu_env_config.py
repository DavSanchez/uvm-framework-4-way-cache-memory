#! /usr/bin/env python
# coding=utf-8

import uvmf_gen

# Genera el entorno
env = uvmf_gen.EnvironmentClass('alu')

# El entorno añade los dos agentes (una interfaz de entrada y otra de salida)
env.addAgent('alu_in_agent', 'alu_in', 'clk', 'rst')
env.addAgent('alu_out_agent', 'alu_out', 'clk', 'rst')

# Define el predictor contenido en este entorno
env.defineAnalysisComponent('predictor', 'alu_predictor',
                            {'alu_in_agent_ae': 'alu_in_transaction #()'},
                            {'alu_sb_ap': 'alu_out_transaction #()'})

# Añade el componente Predictor
env.addAnalysisComponent('alu_pred', 'alu_predictor')

# Añade el componente Scoreboard
env.addUvmfScoreboard(
    'alu_sb', 'uvmf_in_order_scoreboard', 'alu_out_transaction')

# Se especifica el conexionado entre componentes del entorno
env.addConnection('alu_in_agent', 'monitored_ap',
                  'alu_pred', 'alu_in_agent_ae')        # Conexión 0
env.addConnection('alu_pred', 'alu_sb_ap', 'alu_sb',
                  'expected_analysis_export')           # Conexión 1
env.addConnection('alu_out_agent', 'monitored_ap',
                  'alu_sb', 'actual_analysis_export')   # Conexión 2

# Crea el entorno
env.create()
