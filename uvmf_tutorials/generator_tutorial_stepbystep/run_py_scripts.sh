#setenv UVMF_HOME /home/soft/eucad/mentor/2017/questa_sv_af_10.7a_Linux/questasim/examples/UVM_Framework/UVMF_3.6g/
#setenv PYTHONPATH $UVMF_HOME/templates/python/

#echo "$UVMF_HOME"
#echo "$PYTHONPATH"
echo "( time sh run_py_scripts.sh ) > & cmd_output.log"

echo "Ejecutando scripts Python..."
echo "Interfaz de entrada"
time python alu_in_if_config.py

echo "Interfaz de salida"
time python alu_out_if_config.py

echo "Configuración de entorno"
time python alu_env_config.py

echo "Configuración de testbench"
time python alu_bench_config.py

echo "Proceso finalizado"

# Tras esto se creará toda la estructura de directorios
# y se podrá empezar a trabajar sobre el código en
# SystemVerilog y demás cositas!
