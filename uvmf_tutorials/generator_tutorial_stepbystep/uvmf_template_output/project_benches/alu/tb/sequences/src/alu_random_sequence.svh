class alu_random_sequence #(int ALU_IN_OP_WIDTH = 8) extends alu_bench_sequence_base;

    `uvm_object_utils(alu_random_sequence);

    typedef alu_in_reset_sequence #(ALU_IN_OP_WIDTH) alu_reset_sequence_t;
    alu_reset_sequence_t alu_in_reset_s;

    // ************
    function new(string name = "");
        super.new(name);
    endfunction

    //*************
    virtual task body();
        alu_in_agent_random_seq = alu_in_random_sequence#()::type_id::create("alu_in_agent_random_seq");
        alu_in_reset_s = alu_in_reset_sequence#()::type_id::create("alu_in_reset_s");

        alu_in_agent_config.wait_for_reset();
        alu_in_agent_config.wait_for_num_clocks(10);

        repeat (10) alu_in_agent_random_seq.start(alu_in_agent_sequencer);
        alu_in_reset_s.start(alu_in_agent_sequencer);
        repeat (5) alu_in_agent_random_seq.start(alu_in_agent_sequencer);

        alu_in_agent_config.wait_for_num_clocks(50); // 50 = 1000ns/20ns

    endtask

endclass