//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2018 Dec 16
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : alu Environment
// Unit            : Environment infact sequence
// File            : alu_infact_env_sequence.svh
//----------------------------------------------------------------------
//     
// DESCRIPTION: 
// This sequences is a place holder for the infact sequence at the 
// environment level which will generated desired scenarios without redundancy.
//
// ****************************************************************************
// 
class alu_infact_env_sequence extends alu_env_sequence_base;

  // declaration macros
  `uvm_object_utils(alu_infact_env_sequence)

//*****************************************************************
  function new(string name = "");
    super.new(name);
  endfunction: new

endclass: alu_infact_env_sequence
//----------------------------------------------------------------------
//
