//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2018 Dec 16
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : alu_in interface agent
// Unit            : Interface HDL Package
// File            : alu_in_pkg_hdl.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    interface package that needs to be compiled and synthesized
//    for running on Veloce.
//
// CONTAINS:
//    - <alu_in_typedefs_hdl>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package alu_in_pkg_hdl;
  
  import uvmf_base_pkg_hdl::*;

  `include "src/alu_in_typedefs_hdl.svh"

endpackage

