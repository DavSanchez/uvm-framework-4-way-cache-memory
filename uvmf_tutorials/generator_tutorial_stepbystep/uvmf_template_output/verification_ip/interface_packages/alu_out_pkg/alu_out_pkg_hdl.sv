//----------------------------------------------------------------------
//----------------------------------------------------------------------
// Created by      : dsanchez
// Creation Date   : 2018 Dec 16
// Created with uvmf_gen version 3.6g
//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
// Project         : alu_out interface agent
// Unit            : Interface HDL Package
// File            : alu_out_pkg_hdl.sv
//----------------------------------------------------------------------
//     
// PACKAGE: This file defines all of the files contained in the
//    interface package that needs to be compiled and synthesized
//    for running on Veloce.
//
// CONTAINS:
//    - <alu_out_typedefs_hdl>
//
// ****************************************************************************
// ****************************************************************************
//----------------------------------------------------------------------
//
package alu_out_pkg_hdl;
  
  import uvmf_base_pkg_hdl::*;

  `include "src/alu_out_typedefs_hdl.svh"

endpackage

