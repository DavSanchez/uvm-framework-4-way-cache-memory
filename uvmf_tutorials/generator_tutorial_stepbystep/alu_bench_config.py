#! /usr/bin/env python
# coding=utf-8

import uvmf_gen

# Declarando el bench
ben = uvmf_gen.BenchClass('alu', 'alu', {})

# Especificar los agentes que hay en el bench
ben.addBfm('alu_in_agent', 'alu_in', 'clk', 'rst', 'ACTIVE')
ben.addBfm('alu_out_agent', 'alu_out', 'clk', 'rst', 'PASSIVE')

# Crea todos los ficheros del bench
ben.create()
