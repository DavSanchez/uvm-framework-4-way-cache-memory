#! /usr/bin/env python
# coding=utf-8

import uvmf_gen

# El parámetro de esta función es el nombre de la interfaz a crear
intf = uvmf_gen.InterfaceClass('alu_in')
intf.inFactReady = False

# Parámetros para la interfaz
# Usados para definir tamaños de señales y variables
intf.addParamDef('ALU_IN_OP_WIDTH', 'int', '8')

# señales de reloj y reset para la interfaz
intf.clock = 'clk'
intf.reset = 'rst'
intf.resetAssertionLevel = False  # Reset es activo a nivel bajo

# Puertos asociados a la interfaz
# Declarados con respecto al Testbench
intf.addPort('alu_rst', 1, 'output')
intf.addPort('ready', 1, 'input')
intf.addPort('valid', 1, 'output')
intf.addPort('op', 3, 'output')
intf.addPort('a', 'ALU_IN_OP_WIDTH', 'output')
intf.addPort('b', 'ALU_IN_OP_WIDTH', 'output')

# Typedef para incluir en el fichero typedef_hdl
intf.addHdlTypedef('alu_in_op_t',
                   'enum bit[2:0] {no_op=3\'b000, add_op=3\'b001, and_op=3\'b010, xor_op=3\'b011, mul_op=3\'b100, rst_op=3\'b111}')

# Variables de transacción para la interfaz
intf.addTransVar('op', 'alu_in_op_t', isrand=True, iscompare=True)
intf.addTransVar('a', 'bit [ALU_IN_OP_WIDTH-1:0]', isrand=True, iscompare=True)
intf.addTransVar('b', 'bit [ALU_IN_OP_WIDTH-1:0]', isrand=True, iscompare=True)

# Restricción de operaciones
intf.addTransVarConstraint(
    'valid_op_c', '{ op inside {no_op, add_op, and_op, xor_op, mul_op}; }')

# SE CREAN EXPLÍCITAMENTE TODOS LOS FICHEROS SV DE LAS INTERFACES
intf.create()
