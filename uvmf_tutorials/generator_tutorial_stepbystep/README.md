# Generando el entorno de verificación UVMF paso a paso para una ALU

## Ficheros de Python a generar

- [x] Configuración de interfaz de entrada
- [x] Configuración de interfaz de salida
- [x] Configuración del entorno
- [x] Configuración del _bench_
- [x] Añadir cambios necesarios en entorno UVM y DUT
